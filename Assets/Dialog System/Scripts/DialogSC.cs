using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

[CreateAssetMenu(fileName = "DialogSC", menuName = "DialogSC")]
public class DialogSC : ScriptableObject
{
    public List<Dialog> DialogueBlock = new List<Dialog>();

    [System.Serializable]
    public class Dialog
    {
        public Color backgroundColor;
        public bool ChangeBackgroundColor;
        [TextArea]
        public string dialogText;
        public AudioClip clip;
        public float delay = 0;

        public List<Character> LeftCharacters = new List<Character>(6);
        public List<Character> RightCharacters = new List<Character>(6);


    }

    [System.Serializable]
    public class Character
    {
        public Sprite sprite;
        public bool isSpeaker;
        public bool forceActiveColor;
        public bool IsChild;
        //[HideInInspector] public Dialog dialog;
    }

}


