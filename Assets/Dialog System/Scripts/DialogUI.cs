using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Events;

using DG.Tweening;

public class DialogUI : MonoBehaviour
{
    private AudioSource audioSource;
    public DialogSC DialogSC;
    public Image backgroundImagePanel;
    private Color defaultPanelColor;

    public delegate void FinishDialog();
    public event FinishDialog OnFinisDialog;

    public delegate void CurrentDialog(int indexDialog);
    public event CurrentDialog OnCurrentDialog;

    public DialogBlockUI dialogBlockUI;

    public Image DialogContainer;
    public Text DialogText;

    public Sprite DialogContainerLeft;
    public Sprite DialogContainerRight;

    public GameObject DialogLeft;
    public GameObject DialogRight;

    public int currentCount = 0;

    private void Start()
    {
        Initialization();
        SetData(DialogSC.DialogueBlock[0]);
        StartCoroutine(DialogSecuence());


    }

    private void Initialization()
    {
        audioSource = GetComponent<AudioSource>();
        defaultPanelColor = new Color(0, 0, 0, 0.5f);
        dialogBlockUI.Init();
    }


    private IEnumerator DialogSecuence()
    {

        for (int i = currentCount; i < DialogSC.DialogueBlock.Count; i++)
        {
            currentCount = i;
            if (i == 0)
            {
                dialogBlockUI.initFade = true;
            }
            else
                dialogBlockUI.initFade = false;
            OnCurrentDialog?.Invoke(i);
            SetData(DialogSC.DialogueBlock[i]);
            if (DialogSC.DialogueBlock[i].clip != null)
            {
                audioSource.clip = DialogSC.DialogueBlock[i].clip;
                audioSource.Play();
            }
            if (DialogSC.DialogueBlock[i].ChangeBackgroundColor)
            {
                backgroundImagePanel.color = DialogSC.DialogueBlock[i].backgroundColor;
            }
            else
            {
                backgroundImagePanel.color = defaultPanelColor;
            }
            //DialogText.text = DialogSC.DialogueBlock[i].dialogText; //acá reemplazas el texto
            if (DialogSC.DialogueBlock[i].clip != null) yield return new WaitForSeconds(DialogSC.DialogueBlock[i].clip.length + DialogSC.DialogueBlock[i].delay);
            else yield return new WaitForSeconds(DialogSC.DialogueBlock[i].delay);
        }
        OnFinisDialog?.Invoke();
    }

    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.F6))
        {
            StopAllCoroutines();
            currentCount++;
            if (currentCount < DialogSC.DialogueBlock.Count)
                StartCoroutine(DialogSecuence());
            else
            {
                OnFinisDialog?.Invoke();
                gameObject.SetActive(false);
            }
        }

        if (Input.touchCount > 3)
        {
            StopAllCoroutines();
            currentCount++;
            if (currentCount < DialogSC.DialogueBlock.Count)
                StartCoroutine(DialogSecuence());
            else
            {
                OnFinisDialog?.Invoke();
                gameObject.SetActive(false);
            }
        }
    }

    public void SetData(DialogSC.Dialog dialog)
    {

        dialogBlockUI.SetData(dialog);
    }



    public void Close()
    {
        gameObject.SetActive(false);
    }

    public static DialogUI CallDialogUI(DialogSC dialogSC, Transform canvasParent)
    {
        GameObject prefabDialog = (GameObject)Resources.Load("DialogUI");
        GameObject temp = Instantiate(prefabDialog, canvasParent);
        DialogUI dialogUI = temp.GetComponent<DialogUI>();
        dialogUI.DialogSC = dialogSC;

        return dialogUI;
    }

    public static DialogUI CallDialogUI(DialogSC dialogSC, Transform canvasParent, bool startEnable)
    {
        GameObject prefabDialog = (GameObject)Resources.Load("DialogUI");
        GameObject temp = Instantiate(prefabDialog, canvasParent);
        temp.SetActive(false);
        DialogUI dialogUI = temp.GetComponent<DialogUI>();
        dialogUI.DialogSC = dialogSC;

        return dialogUI;
    }


    //CLASSES

    [System.Serializable]
    public class DialogBlockUI
    {
        public DialogGroups LeftGroup;
        public DialogGroups RightGroup;

        public bool initFade;

        public void Init()
        {
            LeftGroup.Init();
            RightGroup.Init();
        }

        public void SetData(DialogSC.Dialog dialog)
        {
            RightGroup.Dialog.transform.GetChild(0).gameObject.SetActive(false);
            RightGroup.Dialog.transform.GetChild(1).gameObject.SetActive(false);
            LeftGroup.Dialog.transform.GetChild(0).gameObject.SetActive(false);
            LeftGroup.Dialog.transform.GetChild(1).gameObject.SetActive(false);
            SetGroupData(dialog, dialog.RightCharacters, RightGroup, "R");
            SetGroupData(dialog, dialog.LeftCharacters, LeftGroup, "L");
        }



        private void SetGroupData(DialogSC.Dialog dialog, List<DialogSC.Character> characters, DialogGroups group, string direction)
        {
            group.Disable();

            TransformGroup GroupImages = group.GetListImage(characters.Count);



            if (GroupImages != null)
            {
                GroupImages.Group.gameObject.SetActive(true);

                for (int i = 0; i < GroupImages.ListCharacters.Count; i++)
                {
                    if (characters[i].sprite != null)
                    {

                        GroupImages.ListCharacters[i].Child.gameObject.SetActive(false);
                        GroupImages.ListCharacters[i].Adult.gameObject.SetActive(false);

                        group.Dialog.transform.GetChild(0).gameObject.SetActive(false);
                        group.Dialog.transform.GetChild(1).gameObject.SetActive(false);

                        if (characters[i].IsChild)
                        {
                            GroupImages.ListCharacters[i].Child.sprite = characters[i].sprite;
                            GroupImages.ListCharacters[i].Child.gameObject.SetActive(true);



                            if (!characters[i].isSpeaker)
                            {
                                GroupImages.ListCharacters[i].Child.color = new Color(0.4f, 0.4f, 0.4f, 1f);
                            }
                            else
                            {
                                GroupImages.ListCharacters[i].Child.color = Color.white;
                                group.Dialog.transform.GetChild(0).gameObject.SetActive(true);
                                group.Dialog.transform.GetChild(0).GetChild(0).GetComponent<Text>().text = dialog.dialogText;
                            }

                            if (characters[i].forceActiveColor)
                            {
                                GroupImages.ListCharacters[i].Child.color = Color.white;
                            }
                        }
                        else
                        {
                            GroupImages.ListCharacters[i].Adult.sprite = characters[i].sprite;
                            GroupImages.ListCharacters[i].Adult.gameObject.SetActive(true);



                            if (!characters[i].isSpeaker)
                            {
                                GroupImages.ListCharacters[i].Adult.color = new Color(0.4f, 0.4f, 0.4f, 1f);
                            }
                            else
                            {
                                GroupImages.ListCharacters[i].Adult.color = Color.white;
                                group.Dialog.transform.GetChild(1).gameObject.SetActive(true);
                                group.Dialog.transform.GetChild(1).GetChild(0).GetComponent<Text>().text = dialog.dialogText;
                            }

                            if (characters[i].forceActiveColor)
                            {
                                GroupImages.ListCharacters[i].Adult.color = Color.white;
                            }
                        }

                    }

                }
            }
            if (initFade && GroupImages != null)
                switch (direction)
                {
                    case "L":

                        FadeLeftImage(GroupImages.ListCharacters[0].Adult);
                        FadeLeftImage(GroupImages.ListCharacters[0].Child);
                        break;
                    case "R":
                        FadeRightImage(GroupImages.ListCharacters[0].Adult);
                        FadeRightImage(GroupImages.ListCharacters[0].Child);
                        break;
                }




        }



        public void FadeLeftImage(Image image)
        {
            Vector3 initPos = image.transform.position - new Vector3(200, 0, 0);
            Vector3 originalPos = image.transform.position;
            image.transform.position = initPos;
            image.transform.DOMove(originalPos, Random.Range(0.5f, 1f)).onComplete += () =>
                {
                    image.transform.position = originalPos;
                    image.transform.DOKill();
                };

            Color originalColor = image.color;
            image.color = new Color(0, 0, 0, 0);
            image.DOColor(originalColor, Random.Range(0.5f, 2.5f)).onComplete += () =>
                {
                    image.color = originalColor;
                    image.DOKill();
                };
        }

        public void FadeRightImage(Image image)
        {
            Vector3 initPos = image.transform.position - new Vector3(-200, 0, 0);
            Vector3 originalPos = image.transform.position;
            image.transform.position = initPos;
            image.transform.DOMove(originalPos, Random.Range(0.5f, 1f)).onComplete += () =>
                {
                    image.transform.position = originalPos;
                    image.transform.DOKill();
                };

            Color originalColor = image.color;
            image.color = new Color(0, 0, 0, 0);
            image.DOColor(originalColor, Random.Range(0.5f, 2.5f)).onComplete += () =>
                {
                    image.color = originalColor;
                    image.DOKill();
                };
        }

    }

    [System.Serializable]
    public class DialogGroups
    {
        public Transform Groups;
        public GameObject Dialog;

        public List<TransformGroup> transformGroups = new List<TransformGroup>();

        public void Init()
        {
            for (int i = 0; i < Groups.childCount; i++)
            {
                TransformGroup temp = new TransformGroup();
                temp.Group = Groups.GetChild(i);
                temp.SetData();
                transformGroups.Add(temp);
            }


        }

        public void Disable()
        {
            foreach (var tg in transformGroups)
            {
                tg.Group.gameObject.SetActive(false);
            }
        }

        public TransformGroup GetListImage(int count)
        {
            if (count - 1 < 0)
            {
                return null;
            }
            else
                return transformGroups[count - 1];
        }

    }

    [System.Serializable]
    public class TransformGroup
    {
        public Transform Group;
        public List<CharacterImage> ListCharacters = new List<CharacterImage>();

        public void SetData()
        {
            for (int i = 0; i < Group.childCount; i++)
            {
                CharacterImage character = new CharacterImage(Group.GetChild(i));
                ListCharacters.Add(character);
            }
        }
    }



    [System.Serializable]
    public class CharacterImage
    {
        public Transform parent;
        public Image Child;
        public Image Adult;

        public CharacterImage(Transform parent)
        {
            this.parent = parent;
            Child = parent.GetChild(0).GetComponent<Image>();
            Adult = parent.GetChild(1).GetComponent<Image>();
        }
    }



}




