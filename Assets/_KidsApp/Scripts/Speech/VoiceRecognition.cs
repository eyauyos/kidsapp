using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;
using TMPro;
using static SpeechRecognizerPlugin;

public class VoiceRecognition : MonoBehaviour, ISpeechRecognizerPlugin
{
    public string TextComparison;
    [SerializeField] private GameObject FinalWindow;
    [SerializeField] private Button startListeningBtn;
    [SerializeField] private GameObject ContinueBtn;
    [SerializeField] private TextMeshProUGUI resultsTxt = null;
    private SpeechRecognizerPlugin plugin = null;

    public UnityAction<bool> OnCorrect;

    private void Start()
    {
        FinalWindow.SetActive(false);

        plugin = SpeechRecognizerPlugin.GetPlatformPluginVersion(this.gameObject.name);
        startListeningBtn.onClick.AddListener(StartListening);
        SetLanguage();
        SetMaxResults();

        OnCorrect += (state) =>
        {
            if (state)
                StartCoroutine(FinishCoroutine());
        };
    }

    private IEnumerator FinishCoroutine()
    {
        yield return new WaitForSeconds(3);
        FinalWindow.SetActive(true);
        yield return new WaitForSeconds(2);
        ContinueBtn.SetActive(true);
    }

    private void StartListening()
    {
        plugin.StartListening();
    }

    private void StopListening()
    {
        plugin.StopListening();
    }

    private void SetContinuousListening(bool isContinuous)
    {
        plugin.SetContinuousListening(isContinuous);
    }

    private void SetLanguage()
    {
        //string newLanguage = languageDropdown.options[dropdownValue].text;
        plugin.SetLanguageForNextRecognition("es-ES");
    }

    private void SetMaxResults()
    {
        // if (string.IsNullOrEmpty(inputValue))
        //     return;

        // int maxResults = int.Parse(inputValue);
        plugin.SetMaxResultsForNextRecognition(0);
    }

    public void OnResult(string recognizedResult)
    {
        char[] delimiterChars = { '~' };
        string[] result = recognizedResult.Split(delimiterChars);
        string finalResult = "";
        resultsTxt.text = "";
        for (int i = 0; i < result.Length; i++)
        {
            finalResult += result[i] + '\n';
        }
        resultsTxt.text = finalResult;

        OnCorrect?.Invoke(TextComparison.Trim() == finalResult.Trim());
    }

    public void OnError(string recognizedError)
    {
        ERROR error = (ERROR)int.Parse(recognizedError);
        switch (error)
        {
            case ERROR.UNKNOWN:
                Debug.Log("<b>ERROR: </b> Unknown");
                //errorsTxt.text += "Unknown";
                break;
            case ERROR.INVALID_LANGUAGE_FORMAT:
                Debug.Log("<b>ERROR: </b> Language format is not valid");
                //errorsTxt.text += "Language format is not valid";
                break;
            default:
                break;
        }
    }
}
