using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Events;

public class TouchHelpController : MonoBehaviour
{
    public float limitBetweenInputs;
    public bool startInputs;

    public int touchCount;
    public int maxTouchs;
    public int fingerCount;

    public UnityAction<int> OnCountTap;
    public AudioSource audioSource;
    public AudioClip tapSound;

    public bool enabledLogic = false;


    void Update()
    {
        if (enabledLogic)
        {
            if (Input.touchCount == fingerCount)
            {
                List<Touch> touches = new List<Touch>();
                bool match = true;

                for (var i = 0; i < fingerCount; i++)
                {
                    touches.Add(Input.GetTouch(i));
                }

                foreach (var item in touches)
                {
                    if (item.phase != TouchPhase.Began)
                    {
                        match = false;
                    }
                }

                if (match)
                {
                    if (!startInputs)
                        StartInputTimer();

                    if (startInputs)
                    {
                        touchCount++;
                        if (touchCount <= maxTouchs)
                            audioSource.PlayOneShot(tapSound);
                    }
                }
            }

        }
    }

    private IEnumerator StartInputTimerCoroutine()
    {
        startInputs = true;
        yield return new WaitForSeconds(limitBetweenInputs);
        if (touchCount <= maxTouchs)
        {
            OnCountTap?.Invoke(touchCount);
        }
        else
        {
            OnCountTap?.Invoke(maxTouchs);
        }
        touchCount = 0;
        startInputs = false;
    }

    public void StartInputTimer()
    {
        StartCoroutine(StartInputTimerCoroutine());
    }

    public void SetTouchDetection(int fingerCount, int maxTouchs, UnityAction<int> OnCountTap)
    {
        enabledLogic = true;
        this.OnCountTap += (count) =>
        {
            OnCountTap?.Invoke(count);
        };
    }

    public static void InstanceTouchDetection(int fingerCount, int maxTouchs, UnityAction<TouchHelpController, int> OnCountTap)
    {
        GameObject touchHelper = Instantiate(Resources.Load("TouchHelper") as GameObject);
        TouchHelpController instance = touchHelper.GetComponent<TouchHelpController>();

        instance.enabledLogic = true;
        instance.OnCountTap += (count) =>
        {
            OnCountTap?.Invoke(instance, count);

        };

        instance.OnCountTap += (count) =>
        {
            if (count == maxTouchs)
            {
                instance.gameObject.SetActive(false);
            }
        };
    }
}
