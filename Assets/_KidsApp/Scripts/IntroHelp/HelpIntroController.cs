using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Events;
using UnityEngine.SceneManagement;
using DG.Tweening;

public class HelpIntroController : MonoBehaviour
{
    public Canvas MainMenuCanvas;
    public Button StartMenuBtn;

    public Canvas FilterCanvas;
    public Button SkipFilterBtn;

    public Canvas HapticFunctionsCanvas;
    public Button SkipHapticBtn;
    public AudioClip HapticAudioclip;

    public TouchHelpController touchHelpController;

    [Header("UI")]
    public List<CanvasVoice> canvasVoices;

    public AudioSource audioSource;



    public UnityAction OnFinishPresentation;

    public UnityAction<string> OnFilterUser;
    // Start is called before the first frame update
    void Start()
    {
        MainMenuCanvas.enabled = true;
        StartMenuBtn.onClick.AddListener(() =>
        {
            FilterCanvas.enabled = true;
            SetupFilterCanvas();
        });

        OnFilterUser += (state) =>
        {
            touchHelpController.gameObject.SetActive(false);
            if (state == "NORMAL")
            {
                SceneManager.LoadScene(1);
            }
            else if (state == "INVIDENTE")
            {
                SceneManager.LoadScene(1);
            }
            else if (state == "SKIP")
            {
                MainMenuCanvas.enabled = false;
                FilterCanvas.enabled = false;
                HapticFunctionsCanvas.enabled = true;
                StopAllCoroutines();
                audioSource.Stop();

                audioSource.clip = HapticAudioclip;
                audioSource.Play();
            }
        };

        SkipHapticBtn.onClick.AddListener(() =>
        {
            audioSource.Stop();
            SceneManager.LoadScene(1);
        });

    }

    public void SetupFilterCanvas()
    {
        foreach (var item in canvasVoices)
        {
            item.canvasGroup.alpha = 0;
        }

        StartPresentation();

        OnFinishPresentation += () =>
        {
            touchHelpController.SetTouchDetection(2, 3,
            (count) =>
            {
                if (count == 2)
                {
                    OnFilterUser?.Invoke("NORMAL");
                }
                else if (count == 3)
                {
                    OnFilterUser?.Invoke("INVIDENTE");
                }
            });
        };

        SkipFilterBtn.onClick.AddListener(() =>
        {
            OnFilterUser?.Invoke("SKIP");
        });
    }

    public void StartPresentation()
    {
        int index = 0;
        StartCanvasVoice(canvasVoices[0]);

        for (var i = 0; i < canvasVoices.Count - 1; i++)
        {
            canvasVoices[i].OnFinish += () =>
            {
                index++;
                StartCanvasVoice(canvasVoices[index]);
            };
        }

        canvasVoices[canvasVoices.Count - 1].OnFinish += () =>
        {
            OnFinishPresentation?.Invoke();
        };
    }

    public void StartCanvasVoice(CanvasVoice canvasVoice)
    {
        StartCoroutine(StartCanvasVoiceCoroutine(canvasVoice));
    }

    public IEnumerator StartCanvasVoiceCoroutine(CanvasVoice canvasVoice)
    {
        canvasVoice.canvasGroup.DOFade(1, 1);
        audioSource.clip = canvasVoice.audioClip;
        audioSource.Play();
        yield return new WaitForSeconds(canvasVoice.audioClip.length);
        canvasVoice.OnFinish?.Invoke();

    }

    [System.Serializable]
    public class CanvasVoice
    {
        public CanvasGroup canvasGroup;
        public AudioClip audioClip;
        public UnityAction OnFinish;
    }
}
