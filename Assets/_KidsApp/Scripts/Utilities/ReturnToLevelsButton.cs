using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class ReturnToLevelsButton : MonoBehaviour
{
    private Button returnBtn;
    // Start is called before the first frame update
    void Awake()
    {
        returnBtn = GetComponent<Button>();

        returnBtn.onClick.AddListener(() =>
        {
            SceneManager.LoadScene(1);
        });
    }
}
