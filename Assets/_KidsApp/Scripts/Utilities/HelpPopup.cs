using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Events;
using DG.Tweening;
using TMPro;

public class HelpPopup : MonoBehaviour
{
    public AudioClip audioClip;
    [Multiline]
    public string MessageStr;
    public TextMeshProUGUI messageText;
    public Button HelpBtn;
    public Button CloseBtn;
    public Canvas HelpCanvas;
    public AudioSource audioSource;
    // Start is called before the first frame update
    void Start()
    {
        messageText.text = MessageStr;
        HelpBtn.onClick.AddListener(() =>
        {
            HelpCanvas.enabled = true;
            audioSource.clip = audioClip;
            audioSource.Play();
        });

        CloseBtn.onClick.AddListener(() =>
        {
            audioSource.Stop();
            HelpCanvas.enabled = false;
        });
    }

}
