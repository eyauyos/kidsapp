using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class ButtonMinigame : MonoBehaviour
{
    public Button MinigameBtn;
    public int SceneIndex;

    // Start is called before the first frame update
    void Start()
    {
        MinigameBtn.onClick.AddListener(() =>
        {
            SceneManager.LoadScene(SceneIndex);
        });
    }
}
