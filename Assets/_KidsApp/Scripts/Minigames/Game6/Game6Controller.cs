using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Events;

public class Game6Controller : MonoBehaviour
{
    public DialogSC initDialog;
    public Canvas canvasDialog;

    public GameObject FollowObject;
    private Gyroscope gyro;
    private Quaternion rotation;
    private bool gyroActive;
    public Quaternion baseRotation;

    public Transform obj;
    public Transform objective1;
    public Transform objective2;

    public float distance1;
    public float distance2;

    public bool findObjective = false;

    public UnityAction OnComplete;

    public float minDistance;

    public bool startGame;

    public Button StartGameBtn;
    public Button ContinueBtn;
    public Transform pivot;

    public Image signal;

    public void EnableGyro()
    {
        if (gyroActive)
            return;

        if (SystemInfo.supportsGyroscope)
        {
            gyro = Input.gyro;
            gyro.enabled = true;
        }
        else
        {
            Debug.Log("gyroscope not supported");
        }

        gyroActive = gyro.enabled;
    }

    private void Awake()
    {
        pivot.gameObject.SetActive(false);
    }

    private void Start()
    {
        DialogUI dialogInitUI = DialogUI.CallDialogUI(initDialog, canvasDialog.transform);
        dialogInitUI.OnFinisDialog += () =>
        {
            dialogInitUI.Close();
        };


        ContinueBtn.gameObject.SetActive(false);
        EnableGyro();
        StartGameBtn.onClick.AddListener(() =>
        {
            StartGameBtn.gameObject.SetActive(false);
            pivot.gameObject.SetActive(true);
            startGame = true;
            pivot.right = -FollowObject.transform.forward;
        });

        OnComplete += () =>
        {
            ContinueBtn.gameObject.SetActive(true);
            pivot.gameObject.SetActive(false);
        };
    }

    // Update is called once per frame
    void Update()
    {
        if (gyroActive)
        {
            rotation = gyro.attitude;
        }

        FollowObject.transform.localRotation = GetGyroRotation() * baseRotation;

        if (startGame)
            SetupGame();

    }

    public void SetupGame()
    {


        distance1 = (obj.position - objective1.position).magnitude;
        distance2 = (obj.position - objective2.position).magnitude;

        if (!findObjective)
        {
            if (distance1 < minDistance)
            {
                findObjective = true;
                OnComplete?.Invoke();
            }

            if (distance1 < minDistance)
            {
                findObjective = true;
                OnComplete?.Invoke();
            }
            signal.fillAmount = (1 / distance1) * 2;
        }

        //x between 0 to var
        //abs(x<1) fill amount = 1

    }

    public Quaternion GetGyroRotation()
    {
        return rotation;
    }
}
