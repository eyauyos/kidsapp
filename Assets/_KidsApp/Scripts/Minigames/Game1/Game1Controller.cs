using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class Game1Controller : MonoBehaviour
{
    public Button continueButton;
    public Button playButton;
    //public Button exitButton;
    public AudioClip clip;
    public AudioSource audioSource;

    public DialogSC initDialog;
    public Canvas canvasDialog;

    void Start()
    {
        Initialize();
        DialogUI dialogInitUI = DialogUI.CallDialogUI(initDialog, canvasDialog.transform);
        dialogInitUI.OnFinisDialog += () =>
        {
            dialogInitUI.Close();
        };
    }

    void Initialize()
    {
        playButton.onClick.AddListener(() =>
        {
            playButton.interactable = false;
            audioSource.clip = clip;
            audioSource.Play();
            StartCoroutine(ActiveContinueButton());
        });
    }

    private IEnumerator ActiveContinueButton()
    {
        yield return new WaitForSeconds(clip.length);
        continueButton.gameObject.SetActive(true);
    }
}
