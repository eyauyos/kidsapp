using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Game2Controller : MonoBehaviour
{
    public DialogSC initDialog;
    public Canvas canvasDialog;
    public GameObject recorder;
    void Start()
    {
        DialogUI dialogInitUI = DialogUI.CallDialogUI(initDialog, canvasDialog.transform);
        dialogInitUI.OnFinisDialog += () =>
        {
            dialogInitUI.Close();
            if (recorder)
                recorder.SetActive(true);
        };
        if (recorder)
            recorder.GetComponent<RecordVoice>().OnContinueAction += () =>
            {
                //Rellenar aca finalizar el juego
            };

    }
}
