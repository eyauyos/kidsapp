using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Game5Controller : MonoBehaviour
{
    public DialogSC initDialog;
    public Canvas canvasDialog;
    public AudioSource audioSource;
    public AudioClip FeedbackComplete;
    public List<Drag> dragObjects;
    public GameObject objective;
    public int countObjs;
    public Button ContinueBtn;
    void Start()
    {
        DialogUI dialogInitUI = DialogUI.CallDialogUI(initDialog, canvasDialog.transform);
        dialogInitUI.OnFinisDialog += () =>
        {
            dialogInitUI.Close();
        };
        ContinueBtn.gameObject.SetActive(false);

        audioSource.clip = FeedbackComplete;
        foreach (var item in dragObjects)
        {
            item.OnTrigger += (obj) =>
            {
                if (obj == null)
                {
                    objective.transform.GetChild(0).gameObject.SetActive(false);
                }
                else if (obj == objective)
                {
                    objective.transform.GetChild(0).gameObject.SetActive(true);
                }
            };
            item.OnContactUp += (obj) =>
            {
                if (obj == objective)
                {
                    countObjs++;
                    item.gameObject.SetActive(false);
                    audioSource.Play();
                }

                if (countObjs == dragObjects.Count)
                {
                    ContinueBtn.gameObject.SetActive(true);
                }
            };
        }
    }
}
