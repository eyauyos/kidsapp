using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PuzzleController : MonoBehaviour
{
    [System.Serializable]
    public class Element {
        public int id_x = 0;
        public int id_y = 0;
        public Sprite image;
    }

    [System.Serializable]
    public class serializableClass
    {
        public List<Element> row;
    }
    public float pieceSizeX;
    public float pieceSizeY;
    public List<serializableClass> sprites; 
    public GameObject spritePositions;
    public GameObject finalPositions;
    public Transform prefabPositions;
    public Transform prefabDragSprite;

    public delegate void AllMatchAction();
    public event AllMatchAction OnAllMatchAction;

    void Start()
    {
        int row = sprites.Count;
        int col = sprites[0].row.Count;
        for(int i = 0; i < row; i++) {
            for(int j = 0; j < col; j++) {
                Vector3 pos = new Vector3((-1*(col-1)/2)*pieceSizeX + j*pieceSizeX, (-1*(row-1)/2)*pieceSizeY + i*pieceSizeY, transform.position.z);
                pos = pos + finalPositions.transform.position;
                var posObject = Instantiate(prefabPositions, pos, transform.rotation, finalPositions.transform);
                posObject.GetComponent<CheckPartMatch>().setId(row-1-i, j);
            }
        }

        for(int i = 0; i < row; i++) {
            for(int j = 0; j < col; j++) {
                Vector3 pos = new Vector3((-1*(col-1)/2)*pieceSizeX + j*pieceSizeX, (-1*(row-1)/2)*pieceSizeY + i*pieceSizeY, transform.position.z);
                pos = pos + spritePositions.transform.position;
                var posObject = Instantiate(prefabDragSprite, pos, transform.rotation, spritePositions.transform);
                posObject.GetComponent<DragPart>().setSprite(sprites[i].row[j].image);
                posObject.GetComponent<DragPart>().setIndex(i, j);
                posObject.GetComponent<Image>().preserveAspect = true;
                // posObject.GetComponent<CheckPartMatch>().setId(i, j);
            }
        }
        shufflePieces();
    }

    private void shufflePieces() {
        int n = spritePositions.transform.childCount;
        for(int i = n-1; i > 0; i--) {
            int j = Random.Range(0, i);
            Vector3 pos = spritePositions.transform.GetChild(i).transform.position;
            spritePositions.transform.GetChild(i).transform.position = spritePositions.transform.GetChild(j).transform.position;
            spritePositions.transform.GetChild(j).transform.position = pos;
        }
        foreach(Transform child in spritePositions.transform) {
            child.GetComponent<DragPart>().initializePosition();
        }
    }
    public void CallAllMatchAction() {
        OnAllMatchAction?.Invoke();
        Debug.Log("Rompecabezas armado");
    }
}
