using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CheckPartMatch : MonoBehaviour
{
    public int id_x;
    public int id_y;
    public bool matched;
    public void setId(int id_x, int id_y) {
        this.id_x = id_x;
        this.id_y = id_y;
    }

    public void OnTriggerEnter2D(Collider2D col) {
        // Debug.Log("chocando");
        DragPart dragPart = col.gameObject.GetComponent<DragPart>();
        int colIDX = dragPart.id_x;
        int colIDY = dragPart.id_y;
        if(id_x == colIDX && id_y == colIDY) {
            dragPart.stopDragging();
            dragPart.setPosition(transform.position.x, transform.position.y);
            matched = true;
            if(checkAllMatched()) {
                transform.parent.parent.GetComponent<PuzzleController>().CallAllMatchAction();
            }
        }
    }
    private bool checkAllMatched() {
        Transform parent = gameObject.transform.parent;
        foreach(Transform child in parent) {
            if(!child.GetComponent<CheckPartMatch>().matched)
                return false;
        }
        return true;
    }
}
