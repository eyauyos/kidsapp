using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;
public class DragPart : MonoBehaviour, IPointerDownHandler, IBeginDragHandler, IEndDragHandler, IDragHandler
{
    private Canvas canvas;
    private RectTransform rectTransform;
    private Vector3 initialPosition;
    //////////////////////////////////////
    public int id_x;
    public int id_y;
    public bool isDragable = false;
    //////////////////////////////////////

    private void Awake() {
        canvas = transform.root.GetComponentInChildren<Canvas>();
        rectTransform = GetComponent<RectTransform>();
    }
    public void OnBeginDrag(PointerEventData eventData) {
        // Debug.Log("OnBeginDrag");
    }

    public void OnDrag(PointerEventData eventData) {
        if(isDragable) {
            // Debug.Log("OnDrag");
            rectTransform.anchoredPosition += eventData.delta/canvas.scaleFactor;
        }
    }

    public void OnEndDrag(PointerEventData eventData) {
        if(isDragable) {
            transform.position = initialPosition;
        }
    }

    public void OnPointerDown(PointerEventData eventData) {
        // Debug.Log("OnPointerDown");
    }
    public void setSprite(Sprite img) {
        GetComponent<Image>().sprite = img;
    }
    public void stopDragging() {
        isDragable = false;
    }
    public void setPosition(float x, float y) {
        transform.position = new Vector3(x, y, transform.position.z);
    }
    public void setIndex(int x, int y) {
        id_x = x;
        id_y = y;
    }
    public void initializePosition() {
        initialPosition = transform.position;
    }
}
