using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

public class DrawBasicController : MonoBehaviour
{

    public UnityAction finishDrawing;

    public SpriteRenderer drawing;

    public string drawingName;
    public Button saveDrawing;


    private void Start()
    {
        saveDrawing.onClick.AddListener(delegate {

            Util.SaveImage(drawing.sprite, drawingName);

            gameObject.SetActive(false);
            finishDrawing?.Invoke();
        });

        Texture2D tex = Util.ReadImage(drawingName);

        if (tex != null) {
            drawing.sprite.texture.SetPixels(tex.GetPixels());
            drawing.sprite.texture.Apply();
        }
    }
}
