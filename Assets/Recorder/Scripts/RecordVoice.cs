using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.UI;
using System;
using System.IO;
using UnityEngine.Events;

[RequireComponent (typeof (AudioSource))]  

public class RecordVoice : MonoBehaviour
{
    public delegate void ContinueAction();
    public event ContinueAction OnContinueAction;
    private bool micConnected = false;
    private int minFreq;
    private int maxFreq;
    public AudioSource goAudioSource;
    [SerializeField]
    private Sprite recordingSprite1;
    [SerializeField]
    private Sprite recordingSprite2;
    [SerializeField]
    private TMP_Text textButtonRecording;
    [SerializeField]
    private Button btnRecording;
    
    [SerializeField] GameObject buttonGroup;
    private double beginTimeRecording;
    private double endTimeRecording;
    private bool startTimer;
    private double currentTime;
    private string pathAudio;
    public GameObject buttons;
    public GameObject panelText;
    public GameObject imageSprite;
    public TMP_Text initialText;
    public AudioClip initialClip;

    public string nameAudio;
    public string initialString;
    public Sprite initialSprite;

    [Header("Others")]
    public Image icon1;
    public Image icon2;

    void Start()
    {
        Initialize();
        SetInitialTextAndAudio();
        StartCoroutine("ActiveButtons");
    }

    private void SetInitialTextAndAudio() {
        buttons.SetActive(false);
        initialText.text = initialString;
        if(initialString == "") {
            panelText.SetActive(false);
        }
        else {
            panelText.SetActive(true);
        }
        if(initialClip != null) {
            goAudioSource.clip = initialClip;
            goAudioSource.Play();
        }
        else {
            Debug.Log("VACIO AUDIO");
        }
        if(initialSprite != null) {
            imageSprite.GetComponent<Image>().sprite = initialSprite;
        }
        else {
            imageSprite.SetActive(false);
        }
    }

    public void SetDoubleImage(Sprite i1, Sprite i2)
    {
        icon1.gameObject.SetActive(true);
        icon1.sprite = i1;
        icon2.gameObject.SetActive(true);
        icon2.sprite = i2;
    }

    private void Initialize()
    {
        pathAudio = Application.persistentDataPath + "/" + nameAudio + ".wav";
        Debug.Log(pathAudio);
        verifyAudioExistence();
        Debug.Log(Application.persistentDataPath);
        if (Microphone.devices.Length <= 0)
        {
            Debug.LogWarning("Microphone not connected!");
        }

        else
        {
            micConnected = true;

            Microphone.GetDeviceCaps(null, out minFreq, out maxFreq);

            if (minFreq == 0 && maxFreq == 0)
            {
                maxFreq = 44100;
            }
            goAudioSource = this.GetComponent<AudioSource>();
        }
    }

    IEnumerator ActiveButtons() {
        if(initialClip != null) {
            yield return new WaitForSeconds(initialClip.length);
        } else {
            yield return new WaitForSeconds(0.0f);
        }
        buttons.SetActive(true);
    }
    void Update() {
        if(startTimer) {
            currentTime += Time.deltaTime;
        }
    }

    public void Record() {
        currentTime = 0;
        startTimer = true;
        textButtonRecording.text = "Suelta para detener grabación.";
        btnRecording.gameObject.GetComponent<Image>().sprite = recordingSprite1;
        goAudioSource.clip = Microphone.Start(null, true, 100, maxFreq);
    }

    public void StopRecording() {
        startTimer = false;
        textButtonRecording.text = initialString;
        btnRecording.gameObject.GetComponent<Image>().sprite = recordingSprite2;
        Microphone.End(null); //Stop the audio recording  
        //save audio
        SavWav.Save(nameAudio, goAudioSource.clip);
        Debug.Log("TIEMPO: " + currentTime);
        // WavFileUtils.TrimWavFile(pathAudio, Path.ChangeExtension(pathAudio,".trimmed.mp3"), TimeSpan.FromSeconds(0), TimeSpan.FromSeconds(99-currentTime));
        WavFileUtils.TrimWavFile(pathAudio, Path.ChangeExtension(pathAudio,".trimmed.wav"), TimeSpan.FromSeconds(0), TimeSpan.FromSeconds(99-currentTime));
        System.IO.File.Delete(pathAudio);
        System.IO.File.Move(Path.ChangeExtension(pathAudio,".trimmed.wav"), pathAudio);
        verifyAudioExistence();
    }

    public void verifyAudioExistence() {
        if (System.IO.File.Exists(pathAudio))
        {
            buttonGroup.SetActive(true); 
        }
        else 
        {
            buttonGroup.SetActive(false);
        }
    }

    public void playCurrentAudio() {
        StartCoroutine(LoadSongCoroutine());
    }


    IEnumerator LoadSongCoroutine()
    {
        string url = string.Format("file://{0}", pathAudio); 
        WWW www = new WWW(url);
        yield return www;
        goAudioSource.clip = www.GetAudioClip(false, false);
        goAudioSource.Play();
    }

    public void deleteCurrentAudio() {
        System.IO.File.Delete(pathAudio);
        verifyAudioExistence();
    }
    public void continueAction() {
        gameObject.SetActive(false);
        OnContinueAction?.Invoke();
    }


}