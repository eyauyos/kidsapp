using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using DG.Tweening;

public class UIController : MonoBehaviour
{
    public static UIController Instance;
    public Sprite HandIcon;
    public Sprite LockIcon;

    [Header("Main Menu Panel")]
    public GameObject mainMenuPanel;
    public Image mainTitleSprite;
    public Button startMainBtn;

    [Header("Register Menu Panel")]
    public GameObject registerMenuPanel;
    public InputField playerNameInputField;
    public GameObject selectedAvatarIcon;
    public List<Button> avatarsBtn;
    public Button nextRegisterBtn;

    [Header("Tutorial Menu Panel")]
    public GameObject tutorialMenuPanel;
    public Button nextTutorialBtn;
    public GameObject buhoImage;
    public float timerFadeInBuho;
    public GameObject colibriImage;
    public float timerFadeInColibri;
    public Button backTutorialBtn;

    [Header("Units Menu Panel")]
    public GameObject unitsMenuPanel;
    public Button unitPrefabBtn;
    public Transform unitContent;
    public List<Sprite> unitActivedIcons;
    public Button backUnitsPanelBtn;

    [Header("Situation Menu Panel")]
    public GameObject situationMenuPanel;
    public Text titleUnit;
    public Text MessageUnit;
    public Button situation1Btn;
    public Text messageSituation1;
    public Image iconSituation1;
    public Button situation2Btn;
    public Text messageSituation2;
    public Image iconSituation2;
    public Button backSituations;

    [Header("Game Menu Panel")]
    public GameObject gameMenuPanel;
    public Button pauseBtn;
    public Button instructionBtn;
    public Button previousGameBtn;
    public Button resetGameBtn;
    public Button nextGameBtn;
    public string instructionTemp;

    //[HideInInspector]
    private List<Button> unitBtnsTemp = new List<Button>();

    private Coroutine tutorialSoundTemp;
    private Coroutine tutorialUITemp;


    private void Awake()
    {
        Instance = this;
    }

    private void Start()
    {
        mainTitleSprite.sprite = AppManager.Instance.data.gradeSprite;

        startMainBtn.onClick.AddListener(delegate { ActivePanel(registerMenuPanel.name); AudioManager.Instance.PlaySound(AudioManager.Instance.RegisterClip); });

        startMainBtn.transform.DOScale(1.2f, 1f).SetLoops(-1, LoopType.Yoyo);

        // REGISTER
        playerNameInputField.onEndEdit.AddListener(CallbackInputField);
        playerNameInputField.onValueChanged.AddListener(CallbackInputField);
        foreach (var avatarBtn in avatarsBtn)
        {
            avatarBtn.onClick.AddListener(delegate { SetPlayerIcon(avatarBtn.transform); });
        }
        nextRegisterBtn.onClick.AddListener(delegate { tutorialUITemp = StartCoroutine(LoadTutorialUI()); });

        // TUTORIAL
        nextTutorialBtn.onClick.AddListener(SkipTutorial);
        backTutorialBtn.onClick.AddListener(BackToRegister);

        // UNITS
        backUnitsPanelBtn.onClick.AddListener(BackToTutorial);

        // UNIT
        situation1Btn.onClick.AddListener(delegate { LoadMiniGame(0); });
        situation2Btn.onClick.AddListener(delegate { LoadMiniGame(1); });
        backSituations.onClick.AddListener(delegate { ActivePanel(unitsMenuPanel.name); });

        //GAME
        pauseBtn.onClick.AddListener(PauseGame);
        instructionBtn.onClick.AddListener(delegate
        {

            MessageManager.Instance.EnableInstructionPanel(instructionTemp);
            AudioManager.Instance.PauseAudio();
        });
        previousGameBtn.onClick.AddListener(RewindMiniGame);
        resetGameBtn.onClick.AddListener(ResetMiniGame);
        nextGameBtn.onClick.AddListener(AdvanceMiniGame);

        //Data
        if (string.IsNullOrEmpty(PlayerPrefs.GetString(AppManager.Instance.data.fileResources)))
        {
            if (Application.platform == RuntimePlatform.WindowsEditor || Application.platform == RuntimePlatform.WindowsPlayer)
                Util.DeleteData();

        }
        else
        {
            playerNameInputField.text = PlayerPrefs.GetString(AppManager.Instance.data.fileResources);
        }

    }


    #region REGISTER PANEL
    private void CallbackInputField(string name)
    {
        AppManager.Instance.playerName = name;
        EnableRegisterBtn();
    }

    private void SetPlayerIcon(Transform parent)
    {
        selectedAvatarIcon.transform.SetParent(parent);
        selectedAvatarIcon.transform.localPosition = Vector3.zero;
        selectedAvatarIcon.SetActive(true);
        AppManager.Instance.playerIcon = parent.GetComponent<Image>().sprite;
        EnableRegisterBtn();
    }


    private void EnableRegisterBtn()
    {
        if (!AppManager.Instance.PlayerNameAndIconIsNull())
        {
            nextRegisterBtn.enabled = true;
            nextRegisterBtn.GetComponent<Image>().color = Color.white;
        }
        else
        {
            nextRegisterBtn.enabled = false;
            nextRegisterBtn.GetComponent<Image>().color = new Color(0.735849f, 0.735849f, 0.735849f);
        }
    }
    #endregion

    #region TUTORIAL
    private IEnumerator LoadTutorialUI()
    {
        PlayerPrefs.SetString(AppManager.Instance.data.fileResources, playerNameInputField.text);

        ResetTutorial();
        ActivePanel(tutorialMenuPanel.name);
        tutorialSoundTemp = StartCoroutine(AudioManager.Instance.PlayTutorial(() => { LoadUnitsToUI(); }));
        yield return new WaitForSeconds(timerFadeInColibri);
        colibriImage.GetComponent<CanvasGroup>().DOFade(1, 1f);
        yield return new WaitForSeconds(timerFadeInBuho);
        buhoImage.GetComponent<CanvasGroup>().DOFade(1, 1f);
    }

    private void SkipTutorial()
    {
        LoadUnitsToUI();
        AudioManager.Instance.StopTutorial();
        StopCoroutine(tutorialSoundTemp);
        StopCoroutine(tutorialUITemp);
        ResetTutorial();
    }

    private void BackToRegister()
    {
        ActivePanel(registerMenuPanel.name);
        AudioManager.Instance.PlaySound(AudioManager.Instance.RegisterClip);
        AudioManager.Instance.StopTutorial();
        StopCoroutine(tutorialSoundTemp);
        StopCoroutine(tutorialUITemp);
        ResetTutorial();
    }

    private void ResetTutorial()
    {
        buhoImage.GetComponent<CanvasGroup>().alpha = 0;
        colibriImage.GetComponent<CanvasGroup>().alpha = 0;
    }
    #endregion

    #region UNITS PANEL
    public void LoadUnitsToUI()
    {
        ActivePanel(unitsMenuPanel.name);

        foreach (var item in unitBtnsTemp)
        {
            Destroy(item.gameObject);
        }

        unitBtnsTemp.Clear();

        var unitsLocal = AppManager.Instance.data.units;

        for (int i = 0; i < unitsLocal.Count; i++)
        {
            var objBtn = Instantiate(unitPrefabBtn, unitContent);
            objBtn.enabled = false;
            objBtn.gameObject.SetActive(true);
            Sprite icon = unitActivedIcons[i];
            objBtn.GetComponent<UnitBtnController>().SetData(icon, unitsLocal[i], LoadUnitToUI);
            unitBtnsTemp.Add(objBtn);
        }

        StartCoroutine(EnableNextUnitToUI(0));

    }

    public IEnumerator EnableNextUnitToUI(float timer)
    {
        yield return new WaitForSeconds(timer);
        int contador = 0;

        for (int i = 0; i < unitBtnsTemp.Count; i++)
        {
            var temp = unitBtnsTemp[i].GetComponent<UnitBtnController>().unitBtn;
            if (AppManager.Instance.FinishedSituations(temp.situations) == false)
            {
                contador = i;
                break;
            }
        }

        unitBtnsTemp[contador].GetComponent<UnitBtnController>().btn.GetComponent<Image>().sprite = unitBtnsTemp[contador].GetComponent<UnitBtnController>().unitIcon;
        unitBtnsTemp[contador].enabled = true;


    }

    public GameObject GetCurrentUnitUIPlayed()
    {
        foreach (var item in unitBtnsTemp)
        {
            var temp = item.GetComponent<UnitBtnController>().unitBtn;
            if (temp == AppManager.Instance.currentUnit)
                return item.gameObject;
        }
        return null;
    }

    private void BackToTutorial()
    {
        tutorialUITemp = StartCoroutine(LoadTutorialUI());
    }
    #endregion


    private void LoadUnitToUI(Unit _unit)
    {
        AppManager.Instance.currentUnit = _unit;

        ActivePanel(situationMenuPanel.name);
        titleUnit.text = AppManager.Instance.currentUnit.title;
        MessageUnit.text = AppManager.Instance.currentUnit.message;

        messageSituation1.text = AppManager.Instance.currentUnit.situations[0].message;
        messageSituation2.text = AppManager.Instance.currentUnit.situations[1].message;

        if (AppManager.Instance.currentUnit.situations[0].icon != null)
        {
            iconSituation1.enabled = true;
            iconSituation1.sprite = AppManager.Instance.currentUnit.situations[0].icon;
        }
        else
            iconSituation1.enabled = false;

        if (AppManager.Instance.currentUnit.situations[1].icon != null)
        {
            iconSituation2.enabled = true;
            iconSituation2.sprite = AppManager.Instance.currentUnit.situations[1].icon;
        }
        else
            iconSituation2.enabled = false;

        EnableSituationsBtn();
    }

    public void EnableSituationsBtn()
    {
        if (AppManager.Instance.currentUnit.situations[0].finishSituation == true)
        {
            situation2Btn.interactable = true;
            // situation2Btn.transform.GetChild(2).gameObject.SetActive(true);
            situation2Btn.transform.GetChild(2).gameObject.GetComponent<Image>().sprite = HandIcon;
        }
        else
        {
            situation2Btn.interactable = false;
            // situation2Btn.transform.GetChild(2).gameObject.SetActive(false);
            situation2Btn.transform.GetChild(2).gameObject.GetComponent<Image>().sprite = LockIcon;
        }
    }

    private void LoadMiniGame(int index)
    {
        MessageManager.Instance.EnableLoading(true);
        ActivePanel(gameMenuPanel.name);
        AppManager.Instance.indexSituation = index;
        AppManager.Instance.LoadMiniGame();

    }

    private void PauseGame()
    {
        MessageManager.Instance.EnablePausePanel(true);
        AudioManager.Instance.PauseAudio();
    }

    //anterior minijuego
    private void RewindMiniGame()
    {
        MessageManager.Instance.EnableLoading(true);
        MessageManager.Instance.EnablePausePanel(false);
        AudioManager.Instance.ResetPauseSystem();
        AppManager.Instance.RewindMiniGame();
    }

    public void ResetMiniGame()
    {
        MessageManager.Instance.EnablePausePanel(false);
        AudioManager.Instance.ResetPauseSystem();
        AppManager.Instance.ResetMiniGame();
    }

    //siguiente minijuego minijuego
    private void AdvanceMiniGame()
    {
        MessageManager.Instance.EnableLoading(true);
        MessageManager.Instance.EnablePausePanel(false);
        AudioManager.Instance.ResetPauseSystem();
        AppManager.Instance.AdvanceMiniGame();
    }

    public void EnableInstructionUI(bool value, string instruction = null)
    {
        instructionBtn.gameObject.SetActive(value);
        instructionTemp = instruction;
    }



    private void ActivePanel(string name)
    {
        mainMenuPanel.SetActive(mainMenuPanel.name.Equals(name));
        registerMenuPanel.SetActive(registerMenuPanel.name.Equals(name));
        tutorialMenuPanel.SetActive(tutorialMenuPanel.name.Equals(name));
        unitsMenuPanel.SetActive(unitsMenuPanel.name.Equals(name));
        situationMenuPanel.SetActive(situationMenuPanel.name.Equals(name));
        gameMenuPanel.SetActive(gameMenuPanel.name.Equals(name));
    }


    public void EnableNextGameBtn(bool v)
    {
        if (v)
        {
            nextGameBtn.enabled = true;
            nextGameBtn.GetComponent<Image>().color = Color.white;
        }
        else
        {
            nextGameBtn.enabled = false;
            nextGameBtn.GetComponent<Image>().color = new Color(0.735849f, 0.735849f, 0.735849f);
        }
    }

    public void EnablePreviousGameBtn(bool v)
    {
        if (v)
        {
            previousGameBtn.enabled = true;
            previousGameBtn.GetComponent<Image>().color = Color.white;
        }
        else
        {
            previousGameBtn.enabled = false;
            previousGameBtn.GetComponent<Image>().color = new Color(0.735849f, 0.735849f, 0.735849f);
        }
    }

    public void ReturnToUnitsUI()
    {
        ActivePanel(unitsMenuPanel.name);
    }

    public void ReturnToSituationsUI()
    {
        ActivePanel(situationMenuPanel.name);
    }
}
