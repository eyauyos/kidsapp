using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;
using UnityEngine.EventSystems;

public class Util
{
    public static void DeleteData()
    {
        string directory = Application.persistentDataPath;
        string[] filePaths = Directory.GetFiles(directory);
        foreach (string filePath in filePaths)
            File.Delete(filePath);
    }

    public static bool InPointerOverUIObject()
    {
        PointerEventData eventDataCurrentPosition = new PointerEventData(EventSystem.current)
        {
            position = new Vector2(Input.mousePosition.x, Input.mousePosition.y)
        };
        List<RaycastResult> results = new List<RaycastResult>();
        EventSystem.current.RaycastAll(eventDataCurrentPosition, results);
        return results.Count > 0;

    }

    public static void SaveImage(Sprite image, string drawingName)
    {
        string path = Application.persistentDataPath + "/" + drawingName + ".png";
        Sprite sprite = image;

        Texture2D croppedTexture = new Texture2D((int)sprite.rect.width, (int)sprite.rect.height);
        var pixels = sprite.texture.GetPixels((int)sprite.textureRect.x, (int)sprite.textureRect.y, (int)sprite.textureRect.width, (int)sprite.textureRect.height);
        croppedTexture.SetPixels(pixels);
        croppedTexture.Apply();

        Debug.Log(path);
        File.WriteAllBytes(path, croppedTexture.EncodeToPNG());
    }

    public static void SaveImage(Texture2D image, string drawingName)
    {
        string path = Application.persistentDataPath + "/" + drawingName + ".png";
        Debug.Log(path);
        File.WriteAllBytes(path, image.EncodeToPNG());
    }

    public static Texture2D ReadImage(string drawingName)
    {
        string path = Application.persistentDataPath + "/" + drawingName + ".png";
        bool exists = File.Exists(path);
        if (exists == true)
        {
            byte[] a = File.ReadAllBytes(path);
            Texture2D tex = new Texture2D(16, 16, TextureFormat.RGB24, false);
            tex.LoadImage(a);
            tex.Apply(true, false);
            return tex;
        }
        else
            return null;
    }

    public static void SaveText(string textName, string content)
    {
        if (string.IsNullOrEmpty(content))
            return;

        string path = Application.persistentDataPath + "/" + textName + ".txt";

        if (File.Exists(path))
            File.Delete(path);

        StreamWriter writer = new StreamWriter(path, true);
        writer.WriteLine(content);
        writer.Close();
        Debug.Log(path);
    }

    public static string ReadText(string textName)
    {
        string path = Application.persistentDataPath + "/" + textName + ".txt";
        bool exists = File.Exists(path);

        if (exists)
        {
            StreamReader reader = new StreamReader(path);
            string a = reader.ReadToEnd();
            reader.Close();
            return a;
        }
        else
            return string.Empty;
    }
}
