using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

public class UnitBtnController : MonoBehaviour
{
    [SerializeField]
    private List<Image> stars;
    public Button btn;


    public Unit unitBtn;

    public Sprite unitIcon;
    public int currentIndex;

    public void SetData(Sprite _unitIcon, Unit _unit, UnityAction<Unit> unityAction)
    {
        unitIcon = _unitIcon;
        unitBtn = _unit;

        btn.onClick.AddListener(() => 
        {
            unityAction?.Invoke(unitBtn);
        });

        StartCoroutine(EnableStars(0));
        EnableBtn();
    }

    private void EnableBtn()
    {
        if (AppManager.Instance.FinishedSituations(unitBtn.situations))
        {
            btn.GetComponent<Image>().sprite = unitIcon;
            btn.enabled = true;
        }
    }

    public IEnumerator EnableStars(float timer)
    {
        yield return new WaitForSeconds(timer);

        for (int i = 0; i < unitBtn.situations.Count; i++)
        {
            if (unitBtn.situations[i].finishSituation)
                stars[i].color = Color.white;
        }
       
    }
}
