using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName ="castellaneando",menuName = "DB castellaneando")]
public class CastellanenadoScriptableObject : ScriptableObject
{
    public string grade;
    public string fileResources;
    public Sprite gradeSprite;
    public List<Unit> units;
}

[System.Serializable]
public class Unit
{
    public string title;
    public string message;
    public List<Situation> situations;
   
}

[System.Serializable]
public class Situation
{
    public string title;
    public string message;
    public Sprite icon;
    public List<MiniGame> miniGames;
    public bool finishSituation;
}

[System.Serializable]
public class MiniGame
{
    //public GameObject game;
    public string gameName;
    public bool finishGame;
}
