using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
public class TakeScreenshoot : MonoBehaviour
{
    public int cont = 0;
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.K)) {
            cont++;
            StartCoroutine(captureScreenshot());
        }
    }
    IEnumerator captureScreenshot()
    {
        yield return new WaitForEndOfFrame();

        string path = Application.persistentDataPath + "/Screenshots/"
                + "_" + cont + "_" + Screen.width + "X" + Screen.height + "" + ".png";

        Texture2D screenImage = new Texture2D(Screen.width, Screen.height);
        //Get Image from screen
        screenImage.ReadPixels(new Rect(0, 0, Screen.width, Screen.height), 0, 0);
        screenImage.Apply();
        //Convert to png
        byte[] imageBytes = screenImage.EncodeToPNG();

        //Save image to file
        System.IO.File.WriteAllBytes(path, imageBytes);
        Debug.Log("CAPTURA TOMADA");
    }
}
