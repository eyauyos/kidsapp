using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using DG.Tweening;
using UnityEngine.UI;


public class TapSound : MonoBehaviour
{
    [System.Serializable]
    public struct ButtonWithSound
    {
        public Button button;
        public AudioClip clip; 
        public bool clicked;
    }
    [SerializeField] private AudioSource audioSource;

    [SerializeField] private float clickScale;

    [SerializeField] public ButtonWithSound[] buttons;

    public delegate void ClickButtonEvent();
    public event ClickButtonEvent OnClickButtonEvent;
    public delegate void ClickButtonEventWithIndex(int index);
    public event ClickButtonEventWithIndex OnClickButtonEventWithIndex;
    public delegate void ClickButtonEventWithObject (GameObject obj);
    public event ClickButtonEventWithObject OnClickButtonEventWithObject;
    public delegate void ClickedAll();
    public event ClickedAll OnClickedAll;
    // void OnEnable() {
    //     for(int i = 0; i < buttons.Length; i++) {
    //         int index = i;
    //         buttons[index].clicked = false;
    //         buttons[index].button.onClick.AddListener(()=>{
    //             // Debug.Log(index);
    //             clickAction(index);
    //         });
    //     }
    // }


    // Test Con Start
    void Start() {
        for(int i = 0; i < buttons.Length; i++) {
            int index = i;
            buttons[index].clicked = false;
            buttons[index].button.onClick.AddListener(()=>{
                // Debug.Log(index);
                clickAction(index);
            });
        }
    }
    private void clickAction(int index) {
        buttons[index].clicked = true;
        DesactiveButtons();
        OnClickButtonEvent?.Invoke();
        OnClickButtonEventWithIndex?.Invoke(index);
        OnClickButtonEventWithObject?.Invoke(buttons[index].button.gameObject);
        // if(CheckAllClicked()) {
        //     OnClickedAll?.Invoke();
        // }
        if (buttons[index].clip != null) {
            audioSource.clip = buttons[index].clip;
            audioSource.Play();
        }
        AnimateClick(index);
    }

    private void AnimateClick(int index)
    {
        if(buttons[index].clip != null) {
            buttons[index].button.transform.DOScale(new Vector3(1,1,1)*clickScale, buttons[index].clip.length/2).OnComplete(()=>{
                buttons[index].button.transform.DOScale(new Vector3(1,1,1), buttons[index].clip.length/2).OnComplete(()=>{
                    ActiveButtons();
                    if(CheckAllClicked()) {
                        OnClickedAll?.Invoke();
                    }
                });
            });
        }
        else {
            ActiveButtons();
            if(CheckAllClicked()) {
                OnClickedAll?.Invoke();
            }
        }
    }
    
    private void ActiveButtons() 
    {
        foreach(var elm in buttons) {
                elm.button.interactable = true;
        }
    }

    private void DesactiveButtons() 
    {
        foreach(var elm in buttons) {
            elm.button.interactable = false;
        }
    } 

    private bool CheckAllClicked() 
    {
        foreach(var elm in buttons) {
            if(!elm.clicked)
            {
                return false;
            }
        }
        return true;
    }
}
