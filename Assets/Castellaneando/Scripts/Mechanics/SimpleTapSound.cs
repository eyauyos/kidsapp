using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Events;
using UnityEngine.EventSystems;
using TMPro;

public class SimpleTapSound : MonoBehaviour, IPointerDownHandler
{
    private AudioSource audioSource;
    private AudioClip dialogAudioClip;
    [SerializeField]private TextMeshProUGUI textClip;

    public delegate void Tap();
    public event Tap Ontap;

    private void Awake()
    {
        if(textClip==null)
        if (transform.childCount>0)
            if (transform.GetChild(0).TryGetComponent<TextMeshProUGUI>(out textClip))
            {
                textClip = transform.GetChild(0).GetComponent<TextMeshProUGUI>();
            }
            else if (transform.childCount>1&&transform.GetChild(1).TryGetComponent<TextMeshProUGUI>(out textClip))
            {
                textClip = transform.GetChild(1).GetComponent<TextMeshProUGUI>();
            }
    }

    public void SetSound(AudioSource audioSource, AudioClip audioClip, string text)
    {
        this.audioSource = audioSource;
        dialogAudioClip = audioClip;
        textClip.text = text;
    }

    public void SetSound(AudioSource audioSource, AudioClip audioClip)
    {
        this.audioSource = audioSource;
        dialogAudioClip = audioClip;
    }

    public void OnPointerDown(PointerEventData eventData)
    {
        if (!audioSource.isPlaying)
        {
            audioSource.clip=dialogAudioClip;
            audioSource.Play();
            Ontap?.Invoke();
        }
    }
}
