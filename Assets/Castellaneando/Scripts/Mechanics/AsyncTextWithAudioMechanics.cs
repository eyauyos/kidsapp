using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;
using DG.Tweening;

public class AsyncTextWithAudioMechanics : MonoBehaviour
{
    public UnityAction finishStoryCallback;

    [Header("Cuento")]
    [TextArea]
    public string story;
    public AudioClip audioStoryClip;
    public List<StoryImage> storyImages;

    [Header("Output")]
    public Text text;
    public Image viewImage;
    public AudioSource audioSource;

    public bool disableObject = true;

    private void Update()
    {
       if (Input.GetKeyDown(KeyCode.P))
       {
           Init();
       }

        if (Input.GetKeyDown(KeyCode.F))
        {
            Debug.Log(audioSource.time);
        }

    }

    public void Init()
    {
        StartCoroutine(InitStory());
    }

    private IEnumerator InitStory()
    {
        audioSource.clip = audioStoryClip;
        audioSource.Play();

        if (viewImage != null)
        {
            viewImage.enabled = false;
            viewImage.DOFade(0, 0);
        }

        if (storyImages.Count > 0)
            StartCoroutine(LoadStoryImage());


        if (!string.IsNullOrEmpty(story))
        {
            char[] whitespace = new char[] { ' ', '\t' };
            string[] ssizes = story.Split(whitespace);

            foreach (char c in story.ToCharArray())
            {
                if (text != null)
                    text.text += c;
                yield return new WaitForSeconds(audioStoryClip.length / (story.Length ));
            }
        }
        else
        {
            yield return new WaitForSeconds(audioStoryClip.length);
        }

        yield return new WaitForSeconds(4f);

        if(disableObject)
            gameObject.SetActive(false);

        Debug.Log("Termino la sincronizacion");
        finishStoryCallback?.Invoke();
    }


    private IEnumerator LoadStoryImage()
    {
        foreach (var storyImage in storyImages)
        {
            yield return new WaitForSeconds(storyImage.timerFadeIn);

            if (storyImage.image != null)
            {
                viewImage.enabled = true;
                viewImage.sprite = storyImage.image;
            }
            if(storyImage.obj!= null)
            {
                storyImage.obj.SetActive(true);
            }

            StartCoroutine(LoadSentence(storyImage.sentence, storyImage.timerSentence));
            viewImage.DOFade(1, 0f);
            yield return new WaitForSeconds(storyImage.timerFadeOut);
            viewImage.DOFade(0, 0f).OnComplete(()=> { viewImage.enabled = false; });
            if (text != null)
                text.text = string.Empty;
            if (storyImage.obj != null)
            {
                storyImage.obj.SetActive(false);
            }

        }
    }

    private IEnumerator LoadSentence(string sentence, float timer)
    {
        char[] whitespace = new char[] { ' ', '\t' };
        string[] ssizes = sentence.Split(whitespace);

        foreach (char c in sentence.ToCharArray())
        {
            if (text != null)
                text.text += c;
            yield return new WaitForSeconds(timer / (sentence.Length + ssizes.Length));
        }
    }
}

[System.Serializable]
public class StoryImage
{
    public float timerFadeIn;
    [TextArea]
    public string sentence;
    public float timerSentence;
    public Sprite image;
    public GameObject obj;
    public float timerFadeOut;
}
