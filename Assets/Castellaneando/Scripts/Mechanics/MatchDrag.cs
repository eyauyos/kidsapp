using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class MatchDrag : MonoBehaviour
{
        public GameObject ObjectTarget;
        public string WALL_META="meta";
        public bool finishBird;
        public delegate void FinishMeta(bool isFinishBird,MatchDrag matchDrag);
        public event FinishMeta OnFinishMeta;
    public delegate void FinishMetaWithTarget (bool isFinishBird,GameObject targetCollision, MatchDrag matchDrag);
    public event FinishMetaWithTarget OnFinishMetaWithTarget;
    public delegate void MistakeMatch();
        public event MistakeMatch OnMistakeMatch;
        public Drag drag;

        private void Start()
        {
            drag = GetComponent<Drag>();
        }

    // Update is called once per frame
    private void OnTriggerEnter2D(Collider2D other)
    {

        if (ObjectTarget != null)
        {
            if (other.gameObject == ObjectTarget)
            {
                OnFinishMeta?.Invoke(finishBird, this);
                OnFinishMetaWithTarget?.Invoke(finishBird, other.gameObject, this);
            }
        }
        else if (other.gameObject.CompareTag(WALL_META))
        {
            OnFinishMeta?.Invoke(finishBird, this);
            OnFinishMetaWithTarget?.Invoke(finishBird, other.gameObject, this);
        }
        else
        {
            OnMistakeMatch?.Invoke();
        }
    }
}    