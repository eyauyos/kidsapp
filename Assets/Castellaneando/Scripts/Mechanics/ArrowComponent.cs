using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ArrowComponent : MonoBehaviour
{
    public GameObject obj;
    public bool isCorrect;
    public GameObject partner;
    public List<GameObject> partners;
    public bool onlyOne = true;
}
