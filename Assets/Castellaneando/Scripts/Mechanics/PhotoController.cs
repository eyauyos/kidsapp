using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public static class PhotoController
{
	public static void TakePicture(UnityAction<Texture2D> picture)
	{

		NativeCamera.Permission permission = NativeCamera.TakePicture((path) =>
		{
			Debug.Log("Image path: " + path);
			if (path != null)
			{
				Texture2D texture = NativeCamera.LoadImageAtPath(path, 512);
				if (texture == null)
				{
					Debug.Log("Couldn't load texture from " + path);
					picture?.Invoke(null);
					return;
				}

				picture?.Invoke(texture);
			}
		}, 512);

		Debug.Log("Permission result: " + permission);
	}
}
