using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System;
using System.Text;
using UnityEngine.Events;

public class WordCorrectionMechanics : MonoBehaviour
{

    public UnityAction finishGameCallback;

    public UnityAction IsCorrectGameCallback;

    public GameObject sentencePrefab;
    public Transform contentSentence;
    public List<Sentence> sentences;
    public bool justificated = false;

    private List<Text> wordstoChange = new List<Text>();
    public Color textChanged;

    public void Init()
    {
        foreach (var sentence in sentences)
        {
            var obj = Instantiate(sentencePrefab, contentSentence);
            Transform content = obj.transform.GetChild(0).transform;
            if (justificated == true)
            {
                content.GetComponent<RectTransform>().anchorMin = new Vector2(0, 0.5f);
                content.GetComponent<RectTransform>().anchorMax = new Vector2(0, 0.5f);
                content.GetComponent<RectTransform>().pivot = new Vector2(0, 0.5f);
            }
            Text textPrefab = content.GetChild(0).GetComponent<Text>();

            foreach (var word in sentence.wordCorrections)
            {
                Text currentText = Instantiate(textPrefab, content);
                currentText.text = word.word + " ";
                if (word.convert)
                {
                    currentText.gameObject.AddComponent<Button>();
                    currentText.GetComponent<Button>().onClick.AddListener(delegate { ChangeWordToUpper(currentText,word.inUpper,word.addEat, word.twoPoints, word.point, word.startQuestionMark, word.endQuestionMark, word.startExclamationMark, word.endExclamationMark); });
                    wordstoChange.Add(currentText);
                   
                }

                currentText.gameObject.SetActive(true);
            }

            obj.gameObject.SetActive(true);
        }
    }

    private void ChangeWordToUpper(Text currentText, bool inUpper, bool addEat, bool twoPoints, bool point, bool startQuestionMark, bool endQuestionMark, bool startExclamationMark, bool endExclamationMark)
    {
        if (inUpper)
        {
            string word = currentText.text;
            string gg = word[0].ToString();
            bool isUpper = char.IsUpper(word[0]);

            if (isUpper)
            {
                currentText.text = gg.ToLower() + word.Remove(0, 1).ToString();
                Debug.Log("<color=red> is incorrect..!!! </color>");

               // MessageManager.Instance?.EnableErrorPanel();
                AudioManager.Instance?.PlaySound(AudioManager.Instance.soundWorng);

            }
            else
            {
                currentText.text = gg.ToUpper() + word.Remove(0, 1).ToString();
                currentText.color = textChanged;
                currentText.GetComponent<Button>().enabled = false;

                if (CheckFinishGame())
                {
                    Debug.Log("<color=green> FINISH GAME </color>");
                    // gameObject.SetActive(false);
                   // MessageManager.Instance?.EnableSuccesPanel(delegate {  });
                    AudioManager.Instance?.PlaySound(AudioManager.Instance.soundGood);

                    StartCoroutine(FinishWordCorrection());
                }
                else
                {
                    Debug.Log("<color=blue> is correct..!!! </color>");

                    //MessageManager.Instance?.EnableSuccesPanel();
                    AudioManager.Instance?.PlaySound(AudioManager.Instance.soundGood);
                    IsCorrectGameCallback?.Invoke();
                }
            }
        }

        if (addEat)
        {
            string word = currentText.text;
            string a = word.Trim();
            char lastLetter = a[a.Length - 1];
            Debug.Log("El ultimo valor: |" + lastLetter + "|");
            if(lastLetter == ',')
            {

            }
            else
            {
                a.Trim(); 
                currentText.text = a + "<size=30>, </size>";
                currentText.color = textChanged;
                currentText.GetComponent<Button>().enabled = false;

                if (CheckFinishGame())
                {
                    Debug.Log("<color=green> FINISH GAME </color>");
                    // gameObject.SetActive(false);
                    finishGameCallback?.Invoke();
                }
                else
                {
                    Debug.Log("<color=blue> is correct..!!! </color>");
                    if (inUpper == false)
                    {
                        //MessageManager.Instance?.EnableSuccesPanel();
                        AudioManager.Instance?.PlaySound(AudioManager.Instance.soundGood);
                        IsCorrectGameCallback?.Invoke();
                    }
                }
            }
        }

        if (twoPoints)
        {
            string word = currentText.text;
            string a = word.Trim();
            char lastLetter = a[a.Length - 1];
            Debug.Log("El ultimo valor: |" + lastLetter + "|");
            if (lastLetter == ':')
            {

            }
            else
            {
                a.Trim();
                currentText.text = a + "<size=30>: </size>";
                currentText.color = textChanged;
                currentText.GetComponent<Button>().enabled = false;

                if (CheckFinishGame())
                {
                    Debug.Log("<color=green> FINISH GAME </color>");
                    // gameObject.SetActive(false);
                    finishGameCallback?.Invoke();
                }
                else
                {
                    Debug.Log("<color=blue> is correct..!!! </color>");
                    if (inUpper == false)
                    {
                       // MessageManager.Instance?.EnableSuccesPanel();
                        AudioManager.Instance?.PlaySound(AudioManager.Instance.soundGood);
                        IsCorrectGameCallback?.Invoke();
                    }
                }
            }
        }

        if (point)
        {
            string word = currentText.text;
            string a = word.Trim();
            char lastLetter = a[a.Length - 1];
            Debug.Log("El ultimo valor: |" + lastLetter + "|");
            if (lastLetter == '.')
            {

            }
            else
            {
                a.Trim();
                currentText.text = a + "<size=30>. </size>";
                currentText.color = textChanged;
                currentText.GetComponent<Button>().enabled = false;

                if (CheckFinishGame())
                {
                    Debug.Log("<color=green> FINISH GAME </color>");
                    // gameObject.SetActive(false);
                    finishGameCallback?.Invoke();
                }
                else
                {
                    Debug.Log("<color=blue> is correct..!!! </color>");
                    if (inUpper == false)
                    {
                        // MessageManager.Instance?.EnableSuccesPanel();
                        AudioManager.Instance?.PlaySound(AudioManager.Instance.soundGood);
                        IsCorrectGameCallback?.Invoke();
                    }
                }
            }
        }

        if (startQuestionMark)
        {
            string word = currentText.text;
            string gg = word[0].ToString();
            //bool isUpper = char.IsUpper(word[0]);

            if (gg == "�")
            {
                //currentText.text = gg.ToLower() + word.Remove(0, 1).ToString();
                Debug.Log("<color=red> is incorrect..!!! </color>");

                // MessageManager.Instance?.EnableErrorPanel();
                AudioManager.Instance?.PlaySound(AudioManager.Instance.soundWorng);

            }
            else
            {
                currentText.text =  "<size=30>�</size>" + word;
                currentText.color = textChanged;
                currentText.GetComponent<Button>().enabled = false;

                if (CheckFinishGame())
                {
                    Debug.Log("<color=green> FINISH GAME </color>");
                    // gameObject.SetActive(false);
                    // MessageManager.Instance?.EnableSuccesPanel(delegate {  });
                    AudioManager.Instance?.PlaySound(AudioManager.Instance.soundGood);

                    StartCoroutine(FinishWordCorrection());
                }
                else
                {
                    Debug.Log("<color=blue> is correct..!!! </color>");

                    //MessageManager.Instance?.EnableSuccesPanel();
                    AudioManager.Instance?.PlaySound(AudioManager.Instance.soundGood);
                    IsCorrectGameCallback?.Invoke();
                }
            }
        }

        if (endQuestionMark)
        {
            string word = currentText.text;
            string a = word.Trim();
            char lastLetter = a[a.Length - 1];
            Debug.Log("El ultimo valor: |" + lastLetter + "|");
            if (lastLetter == '?')
            {

            }
            else
            {
                a.Trim();
                currentText.text = a + "<size=30>? </size>";
                currentText.color = textChanged;
                currentText.GetComponent<Button>().enabled = false;

                if (CheckFinishGame())
                {
                    Debug.Log("<color=green> FINISH GAME </color>");
                    // gameObject.SetActive(false);
                    finishGameCallback?.Invoke();
                }
                else
                {
                    Debug.Log("<color=blue> is correct..!!! </color>");
                    if (inUpper == false)
                    {
                        // MessageManager.Instance?.EnableSuccesPanel();
                        AudioManager.Instance?.PlaySound(AudioManager.Instance.soundGood);
                        IsCorrectGameCallback?.Invoke();
                    }
                }
            }
        }

        if (startExclamationMark)
        {
            string word = currentText.text;
            string gg = word[0].ToString();
            //bool isUpper = char.IsUpper(word[0]);

            if (gg == "�")
            {
                //currentText.text = gg.ToLower() + word.Remove(0, 1).ToString();
                Debug.Log("<color=red> is incorrect..!!! </color>");

                // MessageManager.Instance?.EnableErrorPanel();
                AudioManager.Instance?.PlaySound(AudioManager.Instance.soundWorng);

            }
            else
            {
                currentText.text = "<size=30>�</size>" + word;
                currentText.color = textChanged;
                currentText.GetComponent<Button>().enabled = false;

                if (CheckFinishGame())
                {
                    Debug.Log("<color=green> FINISH GAME </color>");
                    // gameObject.SetActive(false);
                    // MessageManager.Instance?.EnableSuccesPanel(delegate {  });
                    AudioManager.Instance?.PlaySound(AudioManager.Instance.soundGood);

                    StartCoroutine(FinishWordCorrection());
                }
                else
                {
                    Debug.Log("<color=blue> is correct..!!! </color>");

                    //MessageManager.Instance?.EnableSuccesPanel();
                    AudioManager.Instance?.PlaySound(AudioManager.Instance.soundGood);
                    IsCorrectGameCallback?.Invoke();
                }
            }
        }

        if (endExclamationMark)
        {
            string word = currentText.text;
            string a = word.Trim();
            char lastLetter = a[a.Length - 1];
            Debug.Log("El ultimo valor: |" + lastLetter + "|");
            if (lastLetter == '!')
            {

            }
            else
            {
                a.Trim();
                currentText.text = a + "<size=30>! </size>";
                currentText.color = textChanged;
                currentText.GetComponent<Button>().enabled = false;

                if (CheckFinishGame())
                {
                    Debug.Log("<color=green> FINISH GAME </color>");
                    // gameObject.SetActive(false);
                    finishGameCallback?.Invoke();
                }
                else
                {
                    Debug.Log("<color=blue> is correct..!!! </color>");
                    if (inUpper == false)
                    {
                        // MessageManager.Instance?.EnableSuccesPanel();
                        AudioManager.Instance?.PlaySound(AudioManager.Instance.soundGood);
                        IsCorrectGameCallback?.Invoke();
                    }
                }
            }
        }
    }

    private IEnumerator FinishWordCorrection()
    {
        yield return new WaitForSeconds(2f);
        finishGameCallback?.Invoke();
    }

    private bool CheckFinishGame()
    {
        foreach (var word in wordstoChange)
        {
            if (word.GetComponent<Button>().enabled == true)
                return false;
        }

        return true;
    }
}

[System.Serializable]
public class WordCorrection
{
    public string word;
    public bool convert;
    public bool inUpper = true;
    public bool addEat;
    public bool twoPoints;
    public bool point;
    public bool startQuestionMark;
    public bool endQuestionMark;
    public bool startExclamationMark;
    public bool endExclamationMark;
}

[System.Serializable]
public class Sentence
{
    public List<WordCorrection> wordCorrections = new List<WordCorrection>();
}