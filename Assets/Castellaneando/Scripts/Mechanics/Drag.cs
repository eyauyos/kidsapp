using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using DG.Tweening;

public class Drag : MonoBehaviour, IPointerDownHandler, IPointerUpHandler, IPointerExitHandler
{
    public GameObject StartPosition;
    public Vector3 StartPositionVector;
    public Vector3 StartLocalScale;
    public string RETURN_TRIGGER_TAG;
    private bool isDrag = false;
    public float dragScale = 1.1f;
    public bool isEnable = true;
    public bool dragReturn = true;
    //public Vector3 pivot;

    public delegate void DraggingStart();
    public event DraggingStart OnDraggingStart;

    public delegate void DraggingEnd();
    public event DraggingEnd OnDraggingEnd;
    public delegate void DraggingObjEnd(Transform obj);
    public event DraggingObjEnd OnDraggingObjEnd;

    public delegate void ReturnedPosition();
    public event ReturnedPosition OnReturnedPosition;

    public delegate void Returning();
    public event Returning OnReturning;

    public delegate void Trigger(GameObject obj);
    public event Trigger OnTrigger;

    public delegate void ContactUp(GameObject contact);
    public event ContactUp OnContactUp;
    public GameObject currentContact;
    public GameObject lastContact;
    private Rigidbody2D rbd;
    private DG.Tweening.Core.TweenerCore<Vector3, Vector3, DG.Tweening.Plugins.Options.VectorOptions> doScale;
    public int indexDrag;
    private void Start()
    {
        StartPositionVector = transform.position;
        StartLocalScale = transform.localScale;
        rbd = GetComponent<Rigidbody2D>();

        OnDraggingStart += () =>
          {
              DisableExternalDraggers(this);
          };

        OnDraggingEnd += () =>
          {
              EnableExternalDraggers(this);
          };
    }

    public void OnPointerDown(PointerEventData eventData)
    {
        if (isEnable)
        {
            isDrag = true;
            //pivot = Input.mousePosition - transform.position;
            doScale = transform.DOScale(StartLocalScale * dragScale, 0.2f);
            OnDraggingStart?.Invoke();
        }

    }

    private void Update()
    {
        if (isEnable)
        {
            if (isDrag)
            {
                transform.position = Input.mousePosition;
            }
        }

    }

    // private void OnDisable()
    // {
    //     OnDraggingEnd?.Invoke();
    // }

    public void OnPointerUp(PointerEventData eventData)
    {
        if (isEnable)
        {
            isDrag = false;
            if (dragReturn)
            {
                if (StartPosition != null)
                {
                    RestartPosition();
                }
                else
                {
                    RestartPosition(StartPositionVector);
                }

            }

        }
        doScale.Kill();
        doScale = transform.DOScale(StartLocalScale, 0.2f);

        OnDraggingEnd?.Invoke();
        OnDraggingObjEnd?.Invoke(transform);
        if (currentContact != null)
            OnContactUp?.Invoke(currentContact);




    }

    public void OnPointerExit(PointerEventData eventData)
    {
        // if (isEnable)
        // {
        //     isDrag = false;

        // }
        // transform.localScale = new Vector3(1, 1, 1);
    }


    private void OnTriggerEnter2D(Collider2D other)
    {
        if (StartPosition != null)
        {
            if (isEnable)
            {
                if (other.CompareTag(RETURN_TRIGGER_TAG))
                {
                    //transform.position=StartPosition.transform.position;
                    RestartPosition();
                    OnReturning?.Invoke();
                }
            }

        }
        else if (RETURN_TRIGGER_TAG != null && !RETURN_TRIGGER_TAG.Equals(string.Empty))
        {
            if (isEnable)
            {
                if (other.CompareTag(RETURN_TRIGGER_TAG))
                {
                    //transform.position=StartPosition.transform.position;
                    RestartPosition(StartPositionVector);
                    OnReturning?.Invoke();
                }
            }
        }

        currentContact = other.gameObject;
        lastContact = other.gameObject;
        OnTrigger?.Invoke(other.gameObject);

    }

    private void OnTriggerExit2D(Collider2D other)
    {
        if (other.gameObject == currentContact)
        {
            currentContact = null;
            OnTrigger?.Invoke(null);
        }

    }

    public void RestartPosition()
    {
        isDrag = false;
        isEnable = false;

        rbd.simulated = false;
        GetComponent<Image>().raycastTarget = false;
        transform.localScale = StartLocalScale;

        transform.DOMove(StartPosition.transform.position, 1f).onComplete += () =>
           {
               transform.position = StartPosition.transform.position;
               OnReturnedPosition?.Invoke();
               isEnable = true;
               rbd.simulated = true;
               GetComponent<Image>().raycastTarget = true;
               //Debug.Log("position restarted");
           };
    }

    public void RestartPositionInmediately()
    {

        // isEnable=false;
        isDrag = false;
        // rbd.simulated = false;
        // GetComponent<Image>().raycastTarget=false;
        transform.localScale = StartLocalScale;
        // if (StartPosition != null)
        transform.position = StartPositionVector;
        //     transform.position = StartPosition.transform.position;
        // else
        //     transform.position = StartPositionVector;

        OnReturnedPosition?.Invoke();
        isEnable = true;
        rbd.simulated = true;
        GetComponent<Image>().raycastTarget = true;
    }

    public void RestartPosition(Vector3 posStart)
    {
        isDrag = false;
        isEnable = false;

        rbd.simulated = false;
        GetComponent<Image>().raycastTarget = false;
        transform.localScale = StartLocalScale;

        //float distance = (transform.position - posStart).magnitude;
        // if (distance > 100)
        // {
        //     transform.DOLocalMove(posStart, 1f).onComplete += () =>
        //        {
        //            transform.localPosition = posStart;
        //            OnReturnedPosition?.Invoke();
        //            isEnable = true;
        //            rbd.simulated = true;
        //            GetComponent<Image>().raycastTarget = true;
        //        };
        // }
        // else if (distance > 50 && distance < 90)
        // {
        //     transform.DOLocalMove(posStart, 0.5f).onComplete += () =>
        //        {
        //            transform.localPosition = posStart;
        //            OnReturnedPosition?.Invoke();
        //            isEnable = true;
        //            rbd.simulated = true;
        //            GetComponent<Image>().raycastTarget = true;
        //        };
        // }
        // else if (distance < 50)
        // {
        //     transform.DOLocalMove(posStart, 0.05f).onComplete += () =>
        //        {
        //            transform.localPosition = posStart;
        //            OnReturnedPosition?.Invoke();
        //            isEnable = true;
        //            rbd.simulated = true;
        //            GetComponent<Image>().raycastTarget = true;
        //        };
        // }
        transform.DOMove(posStart, 1f).onComplete += () =>
               {
                   transform.position = posStart;
                   OnReturnedPosition?.Invoke();
                   isEnable = true;
                   rbd.simulated = true;
                   GetComponent<Image>().raycastTarget = true;
               };

    }

    public List<Drag> GetExternalDraggers(Drag dragObjectExcluded)
    {
        List<Drag> dragObjs = new List<Drag>(FindObjectsOfType<Drag>());

        dragObjs.Remove(dragObjectExcluded);

        return dragObjs;
    }

    public static void DisableExternalDraggers(Drag dragObjectExcluded)
    {
        List<Drag> dragObjs = new List<Drag>(FindObjectsOfType<Drag>());

        dragObjs.Remove(dragObjectExcluded);

        foreach (var dgo in dragObjs)
        {
            dgo.GetComponent<Image>().raycastTarget = false;
            dgo.enabled = false;
        }
        //Debug.Log("disable external draggers");
    }

    public static void EnableExternalDraggers(Drag dragObjectExcluded)
    {
        List<Drag> dragObjs = new List<Drag>(FindObjectsOfType<Drag>());

        dragObjs.Remove(dragObjectExcluded);

        foreach (var dgo in dragObjs)
        {
            dgo.GetComponent<Image>().raycastTarget = true;
            dgo.enabled = true;
        }
        //Debug.Log("enable external draggers");
    }


}
