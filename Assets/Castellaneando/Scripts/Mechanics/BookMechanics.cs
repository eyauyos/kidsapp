using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;
using TMPro;

public class BookMechanics : MonoBehaviour
{
    public bool isUsingTmpro;
    public UnityAction finishBookCallback;

    public Text pageText;
    public Text pageTex2;
    public TextMeshProUGUI pageText1tmpro;
    public TextMeshProUGUI pageText2tmpro;

    public Button previousBtn;
    public Button nextBtn;
    public int index;
    public Animator book;
    public bool withImage = false;
    public GameObject image;


    public List<PagesBookMechanics> pages;

    public delegate void CurrentIndex(int index);
    public event CurrentIndex OnCurrentIndex;

    private void Start()
    {
        previousBtn.onClick.AddListener(PreviousPage);
        nextBtn.onClick.AddListener(NextPage);

        if (isUsingTmpro)
        {
            pageText1tmpro.text = pages[index].page1;
            pageText2tmpro.text = pages[index].page2;
        }
        else
        {
            pageText.text = pages[index].page1;
            pageTex2.text = pages[index].page2;
        }

        OnCurrentIndex?.Invoke(index);
        previousBtn.gameObject.SetActive(false);
    }

    private void PreviousPage()
    {
        if (index > 0)
        {
            index--;
            // pageText.text = pages[index].page1;
            // pageTex2.text = pages[index].page2;

            if (isUsingTmpro)
            {
                pageText1tmpro.text = pages[index].page1;
                pageText2tmpro.text = pages[index].page2;
            }
            else
            {
                pageText.text = pages[index].page1;
                pageTex2.text = pages[index].page2;
            }

            OnCurrentIndex?.Invoke(index);

            book.SetTrigger("book");
        }

        if (index <= 0)
            previousBtn.gameObject.SetActive(false);

        if (image != null)
            image.SetActive(false);

        //nextBtn.transform.GetChild(0).GetComponent<Text>().text = "Siguiente pagina";
    }

    private void NextPage()
    {
        if (index < pages.Count - 1)
        {
            index++;
            // pageText.text = pages[index].page1;
            // pageTex2.text = pages[index].page2;

            if (isUsingTmpro)
            {
                pageText1tmpro.text = pages[index].page1;
                pageText2tmpro.text = pages[index].page2;
            }
            else
            {
                pageText.text = pages[index].page1;
                pageTex2.text = pages[index].page2;
            }

            OnCurrentIndex?.Invoke(index);
            
            previousBtn.gameObject.SetActive(true);
            book.SetTrigger("book");
        }
        else
        {
            this.gameObject.SetActive(false);
            finishBookCallback?.Invoke();
        }

        if(index == pages.Count - 1)
        {
            Debug.Log("aqui");
            if (image != null)
                image.SetActive(true);
        }

        if (index == pages.Count - 1)
        {
            // if(nextBtn.transform.GetChild(0).gameObject != null)
            //  nextBtn.transform.GetChild(0).GetComponent<Text>().text = "Terminar";
        }
    }

    private void OnDestroy()
    {
        previousBtn.onClick.RemoveAllListeners();
        nextBtn.onClick.RemoveAllListeners();
    }
}

[System.Serializable]
public class PagesBookMechanics
{
    [TextArea]
    public string page1;
    [TextArea]
    public string page2;
}
