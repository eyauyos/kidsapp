using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


[RequireComponent (typeof (Rigidbody2D))]  
[RequireComponent (typeof (MatchDrag))]
[RequireComponent (typeof (Drag))]
public class MatchObject : MonoBehaviour
{
    public MatchDrag matchDragObject;
    private Transform matchPosition;
    
    private Rigidbody2D rbd;
    public Drag dragObject;
    
    private void Awake()
    {
        rbd=GetComponent<Rigidbody2D>();
        dragObject = GetComponent<Drag>();
        matchDragObject = GetComponent<MatchDrag>();  
        rbd.bodyType= RigidbodyType2D.Kinematic;
        //dragObject.StartPositionVector=transform.localPosition;
          
    }

    public void ForceInitialize()
    {
        rbd=GetComponent<Rigidbody2D>();
        dragObject = GetComponent<Drag>();
        matchDragObject = GetComponent<MatchDrag>();  
        rbd.bodyType= RigidbodyType2D.Kinematic;
        //dragObject.StartPositionVector=transform.localPosition;
    }

    public void SetMatchObject(GameObject ObjectTarget)
    {
        matchDragObject.ObjectTarget=ObjectTarget.gameObject;
        matchPosition = ObjectTarget.transform.GetChild(0);
    }

    public void SetMatchPosition()
    {
        matchDragObject.transform.position = matchPosition.position;
        dragObject.enabled=false;
        matchDragObject.GetComponent<Image>().raycastTarget=false;
    }

    public void DestroyDrag()
    {
        Destroy(this);
        Destroy(dragObject);
        Destroy(matchDragObject);
    }

    
}
