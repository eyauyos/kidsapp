using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

public class ImageWithSoundMechanics : MonoBehaviour
{
    public UnityAction finishGameCallback;

    public AudioSource audioSource;
    public List<ImageWithSound> imageWithSounds;

    private ImageWithSound currentImageWithSound;

    private int index;

    public void Start()
    {
        //aaa.text = "<color=#44829f>colorfully</color> awdawda wd";
        foreach (var item in imageWithSounds)
        {
            item.button.onClick.AddListener(delegate { ButtonPressed(item); });
        }
    }

    public void Init()
    {
        StartCoroutine(LoadSound(0));
    }

    private void ButtonPressed(ImageWithSound imageWithSound)
    {
        if(imageWithSound.button == currentImageWithSound.button)
        {
            currentImageWithSound.played = true;
            currentImageWithSound.button.enabled = false;
            currentImageWithSound.button.transform.GetChild(1).gameObject.SetActive(true);
            index++;

            if (!CheckFinishGame()) 
            {
                Debug.Log("<color=blue>correcto...!!!! </color>");
                MessageManager.Instance?.EnableSuccesPanel(()=> { });
                AudioManager.Instance?.PlaySound(AudioManager.Instance.soundGood);
                StartCoroutine(LoadSound(2f));
                //Init();
            }
            else
            {
                Debug.Log("<color=green> FINISH GAMEEE </color>");
                finishGameCallback?.Invoke();
            }
           
           
        }
        else
        {
            Debug.Log("<color=red>incorrecto...!!!! </color>");
            MessageManager.Instance?.EnableErrorPanel();
            AudioManager.Instance?.PlaySound(AudioManager.Instance.soundWorng);
        }
    }

    private IEnumerator LoadSound(float timer)
    {
        yield return new WaitForSeconds(timer);
        EnableButtons(false);

        currentImageWithSound = imageWithSounds[index];
        audioSource.clip = currentImageWithSound.sound;
        audioSource.Play();
        yield return new WaitForSeconds(currentImageWithSound.sound.length);

        EnableButtons(true);
    }

    private void EnableButtons(bool v)
    {
        foreach (var item in imageWithSounds)
        {
            item.button.interactable = v;
        }
    }

    private bool CheckFinishGame()
    {
        foreach (var option in imageWithSounds)
        {
            if (option.played == false)
                return false;
        }

        return true;
    }

}

[System.Serializable]
public class ImageWithSound
{
    public AudioClip sound;
    public bool played;
    public Button button;
}
