using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class JoinWithArrowMechanics : MonoBehaviour
{
    public UnityAction<string> currentIndex;
    public UnityAction finishGameCallback;
    public UnityAction finishCheckCallback;
    public UnityAction finishErrorCallback;
    private GraphicRaycaster m_Raycaster;
    private PointerEventData m_PointerEventData;
    private EventSystem m_EventSystem;

    public LayerMask IgnoreLayer;
    public Material lineMat;
    public List<ArrowComponent> options;

    private ArrowComponent currentMatch;
    private LineRenderer currentLineRender;
    private bool uniting = false;

    private bool initGame;

    public void Init()
    {
        m_Raycaster = GetComponent<GraphicRaycaster>();
        m_EventSystem = GetComponent<EventSystem>();
        initGame = true;
    }

    void Update()
    {
        if (initGame == false)
            return;

        if (Input.GetMouseButtonDown(0))
        {
            foreach (RaycastResult result in resultsRaycast())
            {
                if (result.gameObject.layer != IgnoreLayer)
                {
                    if (result.gameObject.GetComponent<ArrowComponent>() != null)
                    {
                        currentMatch = result.gameObject.GetComponent<ArrowComponent>();

                        if (currentMatch.isCorrect == false)
                        {
                            Vector2 pos2 = Camera.main.ScreenToWorldPoint(result.gameObject.transform.position);

                            GameObject obj = new GameObject();
                            currentLineRender = obj.AddComponent<LineRenderer>();
                            currentLineRender.startWidth = 0.1f;
                            currentLineRender.endWidth = 0.1f;
                            currentLineRender.material = lineMat;
                            currentLineRender.SetPosition(0, pos2);

                            uniting = true;
                        }

                        Debug.Log("siiii");
                    }
                    else
                    {

                        Debug.Log("tambien");

                        if (uniting == false)
                        {
                            Vector2 pos2 = Camera.main.ScreenToWorldPoint(result.gameObject.transform.position);

                            GameObject obj = new GameObject();
                            currentLineRender = obj.AddComponent<LineRenderer>();
                            currentLineRender.startWidth = 0.1f;
                            currentLineRender.endWidth = 0.1f;
                            currentLineRender.material = lineMat;
                            currentLineRender.SetPosition(0, pos2);

                            uniting = true;
                        }
                    }
                }
            }
        }

        if (Input.GetMouseButton(0))
        {
            if (uniting == true)
            {
                Vector2 pos2 = Camera.main.ScreenToWorldPoint(Input.mousePosition);
                currentLineRender.SetPosition(1, pos2);

                foreach (RaycastResult result in resultsRaycast())
                {
                    if (result.gameObject.layer != IgnoreLayer)
                    {
                        if (result.gameObject.GetComponent<ArrowComponent>() != null)
                        {
                            if (currentMatch != null)
                            {
                                if (currentMatch.obj.name != result.gameObject.name)
                                {
                                    uniting = false;
                                    if (currentMatch.onlyOne)
                                    {
                                        if (currentMatch.partner.name == result.gameObject.name)
                                        {
                                            Debug.Log("Es correcto");

                                            Vector2 poscenter = Camera.main.ScreenToWorldPoint(result.gameObject.transform.position);
                                            currentLineRender.SetPosition(1, poscenter);

                                            currentLineRender.transform.SetParent(transform);
                                            MatchSuccess(result.gameObject.name);

                                            if (CheckFinishGame())
                                            {
                                                Debug.Log("<color=green> FINISH GAMEEE </color>");
                                                //gameObject.SetActive(false);
                                                currentIndex?.Invoke(currentMatch.partner.name);
                                                finishGameCallback?.Invoke();
                                            }
                                            else
                                            {
                                                //MessageManager.Instance?.EnableSuccesPanel();
                                                AudioManager.Instance?.PlaySound(AudioManager.Instance.soundGood);
                                                currentIndex?.Invoke(currentMatch.partner.name);
                                                finishCheckCallback?.Invoke();
                                            }

                                        }
                                        else
                                        {
                                            Destroy(currentLineRender.gameObject);
                                            Debug.Log("es incorrecto");
                                            MessageManager.Instance?.EnableErrorPanel();
                                            AudioManager.Instance?.PlaySound(AudioManager.Instance.soundWorng);
                                            finishErrorCallback?.Invoke();

                                        }
                                    }
                                    else
                                    {
                                        bool find = false;
                                        foreach (var item in currentMatch.partners)
                                        {
                                            if (item.name == result.gameObject.name)
                                            {
                                                find = true;
                                                Debug.Log("Es correcto");

                                                Vector2 poscenter = Camera.main.ScreenToWorldPoint(result.gameObject.transform.position);
                                                currentLineRender.SetPosition(1, poscenter);

                                                currentLineRender.transform.SetParent(transform);
                                                MatchSuccess(result.gameObject.name);

                                                if (CheckFinishGame())
                                                {
                                                    Debug.Log("<color=green> FINISH GAMEEE </color>");
                                                    //gameObject.SetActive(false);
                                                    finishGameCallback?.Invoke();
                                                }
                                                else
                                                {
                                                    //MessageManager.Instance?.EnableSuccesPanel();
                                                    AudioManager.Instance?.PlaySound(AudioManager.Instance.soundGood);
                                                    finishCheckCallback?.Invoke();
                                                }

                                            }
                                            // else
                                            // {
                                            //     Destroy(currentLineRender.gameObject);
                                            //     Debug.Log("es incorrecto");
                                            //     MessageManager.Instance?.EnableErrorPanel();
                                            //     AudioManager.Instance?.PlaySound(AudioManager.Instance.soundWorng);
                                            //     finishErrorCallback?.Invoke();

                                            // }
                                        }

                                        if (find==false)
                                        {
                                            Destroy(currentLineRender.gameObject);
                                            Debug.Log("es incorrecto");
                                            MessageManager.Instance?.EnableErrorPanel();
                                            AudioManager.Instance?.PlaySound(AudioManager.Instance.soundWorng);
                                            finishErrorCallback?.Invoke();
                                        }
                                    }

                                    // if (currentMatch.partner.name == result.gameObject.name)
                                    // {
                                    //     Debug.Log("Es correcto");

                                    //     Vector2 poscenter = Camera.main.ScreenToWorldPoint(result.gameObject.transform.position);
                                    //     currentLineRender.SetPosition(1, poscenter);

                                    //     currentLineRender.transform.SetParent(transform);
                                    //     MatchSuccess(result.gameObject.name);

                                    //     if (CheckFinishGame())
                                    //     {
                                    //         Debug.Log("<color=green> FINISH GAMEEE </color>");
                                    //         //gameObject.SetActive(false);
                                    //         finishGameCallback?.Invoke();
                                    //     }
                                    //     else
                                    //     {
                                    //         //MessageManager.Instance?.EnableSuccesPanel();
                                    //         AudioManager.Instance?.PlaySound(AudioManager.Instance.soundGood);
                                    //         finishCheckCallback?.Invoke();
                                    //     }

                                    // }
                                    // else
                                    // {
                                    //     Destroy(currentLineRender.gameObject);
                                    //     Debug.Log("es incorrecto");
                                    //     MessageManager.Instance?.EnableErrorPanel();
                                    //     AudioManager.Instance?.PlaySound(AudioManager.Instance.soundWorng);
                                    //     finishErrorCallback?.Invoke();

                                    // }
                                }
                            }
                        }
                    }
                }
            }
        }

        if (Input.GetMouseButtonUp(0))
        {
            if (uniting)
            {
                Destroy(currentLineRender.gameObject);
                Debug.Log("es incorrecto");
                MessageManager.Instance?.EnableErrorPanel();
                AudioManager.Instance?.PlaySound(AudioManager.Instance.soundWorng);
                uniting = false;
                finishErrorCallback?.Invoke();
            }
        }
    }


    private void MatchSuccess(string partner)
    {
        currentMatch.isCorrect = true;

        foreach (var option in options)
        {
            // if (option.onlyOne)
            // {
                if (option.obj.name == partner)
                    option.isCorrect = true;
            // }
            // else
            // {
            //     foreach (var item in option.partners)
            //     {
            //         if (item.name == partner)
            //         {
            //             option.isCorrect = true;
            //         }
            //     }
            // }

            // else
            // {
            //     List<ArrowComponent> arrowComponents= new List<ArrowComponent>(option.obj.GetComponents<ArrowComponent>());
            //     foreach (var item in arrowComponents)
            //     {
            //         if(item.obj.name==partner)
            //         {

            //         }
            //     }
            // }
        }

    }

    private bool CheckFinishGame()
    {
        foreach (var option in options)
        {
            if (option.isCorrect == false)
                return false;
        }

        return true;
    }

    private List<RaycastResult> resultsRaycast()
    {
        m_PointerEventData = new PointerEventData(m_EventSystem);
        m_PointerEventData.position = Input.mousePosition;
        List<RaycastResult> results = new List<RaycastResult>();
        m_Raycaster.Raycast(m_PointerEventData, results);
        return results;
    }
}
