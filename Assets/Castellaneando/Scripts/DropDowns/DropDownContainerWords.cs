using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Events;

public class DropDownContainerWords : MonoBehaviour
{
    public List<DropDownCheck> dropdowns;
    public GameObject CheckObjImg;
    public bool isComplete;
    public List<string> options;
    public UnityAction<bool> OnCompleteState;

    public void Start()
    {
        foreach (var item in dropdowns)
        {
            item.Init(options);
            item.OnCompleteState += (state) =>
            {
                CheckValues();
            };
        }

        OnCompleteState += (state) =>
        {
            if (state)
            {
                CheckObjImg.SetActive(state);
                AudioManager.Instance?.PlaySound(AudioManager.Instance.soundGood);
            }
            else
            {
                CheckObjImg.SetActive(false);
                //AudioManager.Instance?.PlaySound(AudioManager.Instance.soundWorng);
            }
        };

        //CheckValues();
    }

    private void CheckValues()
    {
        int count = 0;
        foreach (var item in dropdowns)
        {
            if (item.isComplete)
            {
                count++;
            }
        }

        if (count == dropdowns.Count)
        {
            isComplete = true;
            OnCompleteState?.Invoke(isComplete);
        }
        else
        {
            isComplete = false;
            OnCompleteState?.Invoke(isComplete);
        }
    }

    [System.Serializable]
    public class DropDownCheck
    {
        public Dropdown dropdown;
        public List<string> correctValues;
        public bool isComplete;
        public UnityAction<bool> OnCompleteState;

        public void Init(List<string> options)
        {

            // string[] vocalsMayAcent = new string[] { "Á", "É", "Í", "Ó", "Ú" };
            // string[] vocalsMinAcent = new string[] { "á", "é", "í", "ó", "ú" };
            // string[] vocalsMin = new string[] { "a", "e", "i", "o", "u" };
            string[] optionsArr = options.ToArray();
            List<Dropdown.OptionData> optionsList = new List<Dropdown.OptionData>();
            // List<Dropdown.OptionData> vocalsMayus = new List<Dropdown.OptionData>();

            // List<Dropdown.OptionData> vocalsMinusAcent = new List<Dropdown.OptionData>();
            // List<Dropdown.OptionData> vocalsMayusAcent = new List<Dropdown.OptionData>();

            for (int i = 0; i < optionsArr.Length; i++)
            {
                optionsList.Add(new Dropdown.OptionData(optionsArr[i]));
            }

            dropdown.ClearOptions();
            dropdown.AddOptions(optionsList);

            // for (int i = 0; i < vocalsMay.Length; i++)
            // {
            //     vocalsMayus.Add(new Dropdown.OptionData(vocalsMay[i]));
            // }

            // for (int i = 0; i < vocalsMayAcent.Length; i++)
            // {
            //     vocalsMayusAcent.Add(new Dropdown.OptionData(vocalsMayAcent[i]));
            // }

            // for (int i = 0; i < vocalsMinAcent.Length; i++)
            // {
            //     vocalsMinusAcent.Add(new Dropdown.OptionData(vocalsMinAcent[i]));
            // }


            // if (isMayusq && withAcent)
            // {
            //     dropdown.ClearOptions();
            //     dropdown.AddOptions(vocalsMayusAcent);
            // }
            // else if (isMayusq && !withAcent)
            // {
            //     dropdown.ClearOptions();
            //     dropdown.AddOptions(vocalsMayus);
            // }

            // if (!isMayusq && withAcent)
            // {
            //     dropdown.ClearOptions();
            //     dropdown.AddOptions(vocalsMinusAcent);
            // }
            // else if (!isMayusq && !withAcent)
            // {
            //     dropdown.ClearOptions();
            //     dropdown.AddOptions(vocalsMinus);
            // }



            dropdown.onValueChanged.AddListener((call) =>
            {
                CheckComplete();
            });

            CheckComplete();
        }

        private void CheckComplete()
        {
            isComplete = false;
            foreach (var item in correctValues)
            {
                if (dropdown.options[dropdown.value].text == item)
                {
                    isComplete = true;
                }
            }

            OnCompleteState?.Invoke(isComplete);
        }

    }


}

