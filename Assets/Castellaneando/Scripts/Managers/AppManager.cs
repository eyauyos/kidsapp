using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

public class AppManager : MonoBehaviour
{
    public static AppManager Instance;

    public enum StateGame
    {
        start,
        gaming,
        stop
    }
    [Header("Splash Screen")]
    public GameObject panelSplash;

    public StateGame stateGame;

    [Header("Data Base")]
    public CastellanenadoScriptableObject data;

    [Header("User Data")]
    public string playerName;
    public Sprite playerIcon;

    [Header("Unit")]
    public Unit currentUnit;

    [Header("Situation")]
    public int indexSituation;

    [Header("Mini Game")]
    public int currenntIndexMiniGame;
    public MiniGame currentMiniGame;
    public GameObject currentMiniGameObj;

    private int accountant;

    private void Awake()
    {
        Instance = this;
        panelSplash.SetActive(true);



    }

    private void Start()
    {
        //for (int i = 0; i < data.units.Count; i++)
        //{
        //    for (int j = 0; j < data.units[i].situations.Count; j++)
        //    {
        //        for (int h = 0; h < data.units[i].situations[j].miniGames.Count; h++)
        //        {
        //            data.units[i].situations[j].miniGames[h].gameName = data.units[i].situations[j].miniGames[h].game.name;
        //        }
        //    }
        //}

        //EditorUtility.SetDirty(data);

        Screen.sleepTimeout = SleepTimeout.NeverSleep;
    }


    public void RewindMiniGame()
    {
        if (currenntIndexMiniGame > 0)
        {
            currenntIndexMiniGame--;
            StartCoroutine(InstantiateMiniGame(currentUnit.situations[indexSituation].miniGames[currenntIndexMiniGame]));

            EnableNextGameBtn();
            EnablePreviousGameBtn();

        }
    }

    public void ResetMiniGame()
    {
        StartCoroutine(InstantiateMiniGame(currentUnit.situations[indexSituation].miniGames[currenntIndexMiniGame]));
        EnableNextGameBtn();
        EnablePreviousGameBtn();
    }

    public void AdvanceMiniGame()
    {
        if(currenntIndexMiniGame < currentUnit.situations[indexSituation].miniGames.Count)
        {
            currenntIndexMiniGame++;
            StartCoroutine(InstantiateMiniGame(currentUnit.situations[indexSituation].miniGames[currenntIndexMiniGame]));

            EnableNextGameBtn();
            EnablePreviousGameBtn();

            accountant = currenntIndexMiniGame;
        }
    }

    public void LoadMiniGame()
    {
        if (currentUnit.situations[indexSituation].finishSituation == false)
        {
            var mg = GetMiniGame();
            StartCoroutine(InstantiateMiniGame(mg));
        }
        else
        {
            StartCoroutine(InstantiateMiniGame(currentUnit.situations[indexSituation].miniGames[currenntIndexMiniGame]));
        }

        EnableNextGameBtn();
        EnablePreviousGameBtn();
    }

    private void EnableNextGameBtn()
    {
        if (currentUnit.situations[indexSituation].finishSituation == false)
        {
            if (currenntIndexMiniGame >= accountant)
                UIController.Instance.EnableNextGameBtn(false);
            else
                UIController.Instance.EnableNextGameBtn(true);
        }
        else
        {
            if (currenntIndexMiniGame < currentUnit.situations[indexSituation].miniGames.Count - 1)
                UIController.Instance.EnableNextGameBtn(true);
            else
                UIController.Instance.EnableNextGameBtn(false);

        }

    }

    private void EnablePreviousGameBtn()
    {
        if(currentUnit.situations[indexSituation].finishSituation == false)
        {
            if(currenntIndexMiniGame == 0)
                UIController.Instance.EnablePreviousGameBtn(false);
            else
                UIController.Instance.EnablePreviousGameBtn(true);
        }
        else
        {
            if (currenntIndexMiniGame > 0)
                UIController.Instance.EnablePreviousGameBtn(true);
            else
                UIController.Instance.EnablePreviousGameBtn(false);
        }
      

    }

    string nameMiniGame;

    private IEnumerator InstantiateMiniGame(MiniGame miniGame)
    {
        MessageManager.Instance.EnableLoading(true);

        if(currentMiniGameObj!= null)
            Destroy(currentMiniGameObj);

        currentMiniGame = miniGame;
        GameObject obj = Resources.Load<GameObject>(data.fileResources + "/"+ currentMiniGame.gameName);
        obj.SetActive(false);
        currentMiniGameObj = Instantiate(obj, Vector3.zero, Quaternion.identity);
        yield return new WaitForSeconds(2f);
        currentMiniGameObj.SetActive(true);
        nameMiniGame = currentMiniGame.gameName;

        MessageManager.Instance.EnableLoading(false);
    }

    // void OnGUI()
    // {
    //     GUI.Label(new Rect(Screen.width/2, 10, 400, 100), nameMiniGame);
    // }

    private MiniGame GetMiniGame()
    {
        var _minigames = currentUnit.situations[indexSituation].miniGames;

        for (int i = 0; i < _minigames.Count; i++)
        {
            if (_minigames[i].finishGame == false)
            {
                currenntIndexMiniGame = i;
                accountant = i;
                return _minigames[i];
            }
        }
        return null;

    }


    public bool PlayerNameAndIconIsNull()
    {
        if (string.IsNullOrEmpty(playerName) || playerIcon == null)
            return true;

        return false;
    }

    public void FinishMiniGame()
    {
        AudioManager.Instance.PlaySound(AudioManager.Instance.soundGood);
        MessageManager.Instance.EnableCongratulationPanel();
    }

    public void FinishEnableSuccesPanel()
    {
        if (currenntIndexMiniGame >= currentUnit.situations[indexSituation].miniGames.Count - 1)
        {
            currentMiniGame.finishGame = true;
            currentUnit.situations[indexSituation].finishSituation = true;

            ResetValues();

            if(indexSituation == 0)
            {
                UIController.Instance.ReturnToSituationsUI();
                MessageManager.Instance.ResetStars();
                UIController.Instance.EnableSituationsBtn();
            }
            else
            {
                UIController.Instance.ReturnToUnitsUI();
                MessageManager.Instance.ResetStars();
                var obj = UIController.Instance.GetCurrentUnitUIPlayed();
                StartCoroutine(obj.GetComponent<UnitBtnController>().EnableStars(1f));
                StartCoroutine(UIController.Instance.EnableNextUnitToUI(1f));
            }

            //if (FinishedSituations(currentUnit.situations) == false)
            //{
            //    UIController.Instance.ReturnToSituationsUI();
            //    UIController.Instance.EnableSituationsBtn();
            //}
            //else
            //{
            //    UIController.Instance.ReturnToUnitsUI();
            //    var obj = UIController.Instance.GetCurrentUnitUIPlayed();
            //    StartCoroutine(obj.GetComponent<UnitBtnController>().EnableStars(1f));
            //    StartCoroutine(UIController.Instance.EnableNextUnitToUI(1f));
            //}

            Debug.Log("termino todo");
        }
        else
        {
            currentMiniGame.finishGame = true;
            AdvanceMiniGame();
        }
    }

    public void ResetValues()
    {
        currenntIndexMiniGame = 0;
        currentMiniGame = null;
        if (currentMiniGameObj != null)
            Destroy(currentMiniGameObj);
    }

    public bool FinishedSituations(List<Situation> situations)
    {
        foreach (var situation in situations)
        {
            if (situation.finishSituation == false)
                return false;
        }

        return true;
    }
}
