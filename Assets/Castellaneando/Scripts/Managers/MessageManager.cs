using DG.Tweening;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;
using DG.Tweening;

public class MessageManager : MonoBehaviour
{
    public static MessageManager Instance;

    [Header("Loading")]
    public GameObject loadingPanel;
    
    public Sprite starSprite;
    public Sprite StopSprite;

    public GameObject successPanel;
    public GameObject successFinishPanel;
    public Image viewPhoto;
    public Text userNameText;
    public GameObject errorPanel;

    [Header("pausa Panel")]
    public GameObject pausePanel;
    public Button continueBtn;
    public Button unitsBtn;
    public Button exitGameBtn;
    public List<Image> starsPause;

    [Header("Congratulation Panel")]
    public GameObject congratulationPanel;
    public Button nextBtn;
    public Image starEarned;
    public List<Image> starsCongra;

    [Header("Instruction Panel")]
    public GameObject instructionPanel;
    public Text intructionText;
    public Button closeIntructionBtn;

    private void Awake()
    {
        Instance = this;

        continueBtn.onClick.AddListener(ContinueGame);
        unitsBtn.onClick.AddListener(ReturnUnits);
        exitGameBtn.onClick.AddListener(delegate { Application.Quit(); });

        closeIntructionBtn.onClick.AddListener(delegate { 
            instructionPanel.SetActive(false);
            AudioManager.Instance.ContinueAudio();
        });

    }

    private void Start()
    {
        nextBtn.onClick.AddListener(()=> {
            AppManager.Instance.FinishEnableSuccesPanel();
            congratulationPanel.SetActive(false);
        });
    }

    public void EnableLoading(bool value)
    {
        loadingPanel.SetActive(value);
    }

    public void EnableInstructionPanel(string info)
    {
        intructionText.text = info;
        instructionPanel.SetActive(true);
    }

    public void EnableSuccesPanel(UnityAction closePanel= null)
    {
        StartCoroutine(WaitingEnableSuccesPanel(closePanel));
    }

    private void EnableSuccesFinishPanel(UnityAction closePanel = null)
    {
        StartCoroutine(WaitingEnableSuccesFinishPanel(closePanel));
    }

    public void EnableCongratulationPanel()
    {
        EnableSuccesFinishPanel(delegate { StartCoroutine(WaitingCongratulationPanel()); });
    }

    private IEnumerator WaitingEnableSuccesPanel(UnityAction closePanel = null)
    {
        yield return new WaitForSeconds(0f);
        successPanel.SetActive(true);
        StartCoroutine(WaitingDisablePanel(successPanel, 3f, closePanel));
    }

    private IEnumerator WaitingEnableSuccesFinishPanel(UnityAction closePanel = null)
    {
        yield return new WaitForSeconds(0f);
        successFinishPanel.SetActive(true);
        viewPhoto.sprite = AppManager.Instance.playerIcon;
        userNameText.text = AppManager.Instance.playerName;
        StartCoroutine(WaitingDisablePanel(successFinishPanel, 3f, closePanel));
    }

    private IEnumerator WaitingCongratulationPanel()
    {
        yield return new WaitForSeconds(0f);
        congratulationPanel.SetActive(true);
        StarsCongratulationPanel();
       // StartCoroutine(WaitingDisablePanel(congratulationPanel, 5f, closePanel));
    }

    public void EnableErrorPanel()
    {
        StartCoroutine(WaitingEnableErrorPanel());
    }

    private IEnumerator WaitingEnableErrorPanel()
    {
        yield return new WaitForSeconds(0f);
        errorPanel.SetActive(true);
        StartCoroutine(WaitingDisablePanel(errorPanel, 1f));
    }

    public void EnablePausePanel(bool value)
    {
        pausePanel.SetActive(value);
        StarsPausePanel();
    }

    private void StarsPausePanel()
    {
        for (int i = 0; i < AppManager.Instance.currenntIndexMiniGame+1; i++)
        {
            starsPause[i].sprite = starSprite;
        }
    }


    public void ResetStars()
    {
        starEarned.color = new Color(0.735849f, 0.735849f, 0.735849f);
        DOTween.Kill(starEarned);

        foreach (var item in starsCongra)
        {
            item.sprite = StopSprite;
        }
    }

    private void StarsCongratulationPanel()
    {
        for (int i = 0; i < AppManager.Instance.currenntIndexMiniGame + 1; i++)
        {
            starsCongra[i].sprite = starSprite;
        }

        if(AppManager.Instance.currenntIndexMiniGame >= AppManager.Instance.currentUnit.situations[AppManager.Instance.indexSituation].miniGames.Count-1)
        {
            starEarned.color = Color.white;
            starEarned.transform.DOScale(1.2f, 1f).SetLoops(-1, LoopType.Yoyo);
        }
        else
        {
            starEarned.color = new Color(0.735849f, 0.735849f, 0.735849f);
            DOTween.Kill(starEarned);
        }
    }

    private void ContinueGame()
    {
        EnablePausePanel(false);
        AudioManager.Instance.ContinueAudio();
    }

    private void ReturnUnits()
    {
        EnablePausePanel(false);
        AudioManager.Instance.ResetPauseSystem();
        AppManager.Instance.ResetValues();
        UIController.Instance.LoadUnitsToUI();
    }

    private IEnumerator WaitingDisablePanel(GameObject obj, float timer, UnityAction closePanel = null)
    {
        yield return new WaitForSeconds(timer);
        obj.SetActive(false);
        closePanel?.Invoke();
    }
}
