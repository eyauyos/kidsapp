using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class AudioManager : MonoBehaviour
{
    public static AudioManager Instance;

    public AudioSource AudioSourceManager;

    public AudioSource backgroundMusicAudiosource;
    [Header("Sounds")]
    public AudioClip soundGood;
    public AudioClip soundWorng;

    [Header("Tutorial")]
    public AudioClip tutorial;

    [Header("Tutorial")]
    public AudioClip RegisterClip;

    public List<AudioSource> audiosPlaying = new List<AudioSource>();

    private void Awake()
    {
        Instance = this;

    }

    public void PlaySound(AudioClip audioClip)
    {
        StartCoroutine(WaitingPlaySound(audioClip));
    }

    private IEnumerator WaitingPlaySound(AudioClip audioClip)
    {
        yield return new WaitForSeconds(0.2f);
        AudioSourceManager.clip = audioClip;
        AudioSourceManager.Play();
    }

    public IEnumerator PlayTutorial(UnityAction finishTutorialCallback = null)
    {
        AudioSourceManager.clip = tutorial;
        AudioSourceManager.Play();
        yield return new WaitForSeconds(tutorial.length);
        finishTutorialCallback?.Invoke();
    }

    public void StopTutorial()
    {
        AudioSourceManager.Stop();
    }


    public void PauseAudio()
    {


        var objs = FindObjectsOfType<AudioSource>();
        foreach (var item in objs)
        {
            Debug.Log("dawwwwwwwwwwwwwwwwwwwwwwwwww");
            if (item.isPlaying && item != backgroundMusicAudiosource)
            {
                item.Pause();
                audiosPlaying.Add(item);
            }
        }

        Time.timeScale = 0;
    }

    public void ContinueAudio()
    {
        Time.timeScale = 1;

        foreach (var item in audiosPlaying)
        {
            Debug.Log("volver a reproducir");
            item.Play();

        }

        audiosPlaying.Clear();
    }

    public void ResetPauseSystem()
    {
        Time.timeScale = 1;
        audiosPlaying.Clear();
    }

}
