using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameDemoController : MonoBehaviour
{
    public bool finishGame;

    public AudioSource audioSource;
    public AudioClip dialogoProfesora;

    void Update()
    {
        if (Input.GetKeyDown(KeyCode.F))
        {
            if(finishGame== false)
            {
                //Llamas a este evento cuando termines el juego
                AppManager.Instance.FinishMiniGame();

                finishGame = true;
            }
        }

        if (Input.GetKeyDown(KeyCode.E))
        {
            //Llamas a estos eventos cuando te equivocas en un ejercicio del juego
            MessageManager.Instance.EnableErrorPanel();
            AudioManager.Instance.PlaySound(AudioManager.Instance.soundWorng);
        }

        if (Input.GetKeyDown(KeyCode.C))
        {
            //Llamas a estos eventos cuando haces bien un ejercicio
            MessageManager.Instance.EnableSuccesPanel();
            AudioManager.Instance.PlaySound(AudioManager.Instance.soundGood);
        }

        //if (Input.GetKeyDown(KeyCode.P))
        //{
        //    //Llamas a estos eventos cuando haces bien un ejercicio
        //    if (audioSource != null)
        //    {
        //        audioSource.clip = dialogoProfesora;
        //        audioSource.Play();
        //    }
        //}



    }
}
